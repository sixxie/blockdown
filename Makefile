# Blockdown - falling blocks puzzle game for Dragon/CoCo 16K+
# Copyright (c) 2020-2022 Ciaran Anscomb

default: tape rom
.PHONY: default

tape: blockdown.cas blockdown.wav
.PHONY: tape

disk: blockdown.dsk
.PHONY: disk

rom: blockdown.rom
.PHONY: rom

all: tape disk rom
.PHONY: all

####

ASM6809 = asm6809 -v
BIN2CAS = bin2cas.pl
B2CFLAGS = -r 44100
CLEAN =
EXTRA_DIST =

####

tile2bin: CFLAGS += $(shell sdl-config --cflags)
tile2bin: LDLIBS += $(shell sdl-config --libs) -lSDL_image

CLEAN += tile2bin

####

%.bin: %.s
	$(ASM6809) $(AFLAGS) -l $(<:.s=.lis) -o $@ $<

%.dz: %
	dzip -c $< > $@

%.dz8: %
	dzip -8 -c $< > $@

####

piece-data.s: piece-data.pl
	./piece-data.pl > $@

CLEAN += piece-data.s
EXTRA_DIST += piece-data.s

font-askew.s: font.pl
	./font.pl --vdg > $@

font-askew-t1.s: font.pl
	./font.pl --t1 > $@

CLEAN += font-askew.s font-askew-t1.s
EXTRA_DIST += font-askew.s font-askew-t1.s

playscreen-1up.bin: playscreen-top.bin tile2bin
playscreen-1up.bin: playscreen-1up.png
	cp playscreen-top.bin $@
	./tile2bin -S $< >> $@

playscreen-2up.bin: playscreen-top.bin tile2bin
playscreen-2up.bin: playscreen-2up.png
	cp playscreen-top.bin $@
	./tile2bin -S $< >> $@

CLEAN += playscreen-1up.bin playscreen-2up.bin

blockdown.bin: dragonhw.s dunzip8.s font-askew.s font-askew-t1.s font-score.s
blockdown.bin: kbd.s pieces.s piece-data.s players.s rnd.s snd.s start.s
blockdown.bin: title.s vdg.s
blockdown.bin: playscreen-1up.bin.dz8 playscreen-2up.bin.dz8
blockdown.bin: AFLAGS = -C

CLEAN += playscreen-1up.bin.dz8 playscreen-2up.bin.dz8
CLEAN += blockdown.bin blockdown.lis

EXTRA_DIST += playscreen-1up.bin.dz8 playscreen-2up.bin.dz8

# Depend on blockdown.bin for this so we get all its dependencies
blockdown-rom.bin: blockdown.bin

blockdown-rom.bin: blockdown.s
	$(ASM6809) -B -l $(@:.bin=.lis) -E $(@:.bin=.exp) -o $@ $<

CLEAN += blockdown-rom.bin blockdown-rom.exp blockdown-rom.lis

####

loadscreen1.bin: tile2bin
loadscreen1.bin: loadscreen.png
	./tile2bin -S -h 64 -o $@ $<

loadscreen2.bin: tile2bin
loadscreen2.bin: loadscreen.png
	./tile2bin -S -y 64 -h 26 -o $@ $<
	perl -e '$$a="     (C) 2021  TEIPEN MWNCI     ";$$a=~tr/\x40-\x5f/\x00-\x1f/; print $$a x 6' >> $@

CLEAN += loadscreen1.bin loadscreen2.bin

loadscreen-rom.bin: tile2bin
loadscreen-rom.bin: loadscreen.png
	./tile2bin -S -h 90 -o $@ $<
	perl -e '$$a="     (C) 2021  TEIPEN MWNCI     ";$$a=~tr/\x40-\x5f/\x00-\x1f/; print $$a x 6' >> $@

CLEAN += loadscreen-rom.bin

####

disk-boot.bin: AFLAGS = -B -l disk-boot.lis

CLEAN += disk-boot.bin disk-boot.lis

####

blockdown.cas blockdown.wav: loadscreen1.bin loadscreen2.bin blockdown.bin
	$(BIN2CAS) $(B2CFLAGS) --cue --autorun --eof-data --fast -o $@ -n "BLOCKD" \
		-B -l 0x0400 --zload 0x1000 --dzip loadscreen1.bin \
		--vdg 8 --sam-v 2 --sam-f 2 --poke 0xff9c,8 \
		-B -l 0x0c00 --zload 0x1000 --dzip loadscreen2.bin \
		--sam-v 4 --poke 0xff9c,9 \
		-C --dzip blockdown.bin

CLEAN += blockdown.cas blockdown.wav

blockdown.dsk: hybrid.dsk blockdown.bin disk-boot.bin
	rm -f $@
	cp hybrid.dsk $@
	dd if=disk-boot.bin of=$@ bs=256 seek=2 conv=notrunc
	dd if=disk-boot.bin of=$@ bs=256 seek=$(shell expr 34 \* 18) conv=notrunc
	decb copy -2b blockdown.bin $@,BLOCKD.BIN
	@echo Copy Dragon files on manually for now

CLEAN += blockdown.dsk

####

blockdown.rom: dragonhw.s
blockdown.rom: loadscreen-rom.bin.dz blockdown-rom.bin.dz

CLEAN += loadscreen-rom.bin.dz blockdown-rom.bin.dz

blockdown.rom: romcart.s
	$(ASM6809) -B -l $(<:.s=.lis) -o $@ $<

CLEAN += blockdown.rom romcart.lis

####

clean:
	rm -f $(CLEAN)
.PHONY: clean
