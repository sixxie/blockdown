; Random number generation and bag shuffling.

		section "DEFS"

POLYMASK32	equ $b4bcd35c
POLYMASK31	equ $7a5bc2e3

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DP"

rnd_lfsr32	rmb 4
rnd_lfsr31	rmb 4
rnd_state	rmb 1
rnd_next	rmb 1		; next pool fetch index
rnd_tmp0	rmb 1		; only need 1 byte
rnd_tmp_bagptr	rmb 2

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		BSS

; Random pool is initialised in the INIT section in start.s.  1 extra byte
; is allocated, as it is filled 2 bytes at a time, but rnd_next may be
; changed by only 1, and this can end up storing 1 byte past the pool.

rndpool		equ *+128
		rmb 256+1

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; Call this as often as possible to keep the random pool filled.  Two
; bytes are generated every four calls.

rnd_generate
		lda rnd_state
		inca
		anda #3
		sta rnd_state

		deca
		beq shift_lfsr32
		deca
		beq shift_lfsr32
		deca
		beq shift_lfsr31

		ldx #rndpool
		lda rnd_next
		leax a,x
		adda #2
		sta rnd_next

		; Exclusive OR lower 16 bits of both LFSRs for 2 random
		; bytes.  Fill the pool.
		ldd rnd_lfsr32+2
		eora rnd_lfsr31+2
		eorb rnd_lfsr31+3
		std ,x
		rts

shift_lfsr32	lsr rnd_lfsr32
		ror rnd_lfsr32+1
		ror rnd_lfsr32+2
		ror rnd_lfsr32+3
		bcc >
		ldd #(POLYMASK32>>16)&$ffff
		eora rnd_lfsr32
		eorb rnd_lfsr32+1
		std rnd_lfsr32
		ldd #POLYMASK32&$ffff
		eora rnd_lfsr32+2
		eorb rnd_lfsr32+3
		std rnd_lfsr32+2
!		rts

shift_lfsr31	lsr rnd_lfsr31
		ror rnd_lfsr31+1
		ror rnd_lfsr31+2
		ror rnd_lfsr31+3
		bcc >
		ldd #(POLYMASK31>>16)&$ffff
		eora rnd_lfsr31
		eorb rnd_lfsr31+1
		std rnd_lfsr31
		ldd #POLYMASK31&$ffff
		eora rnd_lfsr31+2
		eorb rnd_lfsr31+3
		std rnd_lfsr31+2
!		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; Entry:
; 	B = limit (2-255 are useful)
; Exit:
; 	A = random number from 0 to B-1

rnd_num		pshs x
		dec rnd_next
		ldx #rndpool
		lda rnd_next
		lda a,x
		mul
		puls x,pc

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; Takes a piece from the bag.  If the bag is "empty", re-shuffle it and
; mark it "full".

; Entry:
; 	U -> player
; Exit:
; 	X -> new piece

rnd_piece	ldb plr_bag_npc,u
		bne 100F

		; bag empty, so shuffle it (fisher-yates)
		pshs y,u
		leax plr_bag,u
		stx rnd_tmp_bagptr
		leau plr_bag+6*2,u
		ldb #6
10		stb rnd_tmp0
		incb
		bsr rnd_num
		lsla
		ldx rnd_tmp_bagptr
		leay a,x
		ldd ,y
		ldx ,u
		std ,u
		stx ,y
		leau -2,u
		ldb rnd_tmp0
		decb
		bne 10B
		puls y,u
		ldb #7		; new number of pieces

		; next piece from end of bag
100		decb
		stb plr_bag_npc,u
		lslb
		leax plr_bag,u
		ldx b,x
		rts

