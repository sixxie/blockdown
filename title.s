; Title page, options, etc.

; Note: using FCI for strings embeds them internally as inverse-video VDG
; values.  That would lead to '@' becoming a zero byte.  Nothing here
; contains an '@', and high score names are printed a character at a time,
; so we're fine for now, but be aware!

		section "DEFS"

SCN_JSR		macro
		fcb $fd
		fdb \1
		endm

SCN_CALL	macro
		fcb $fe
		fdb \1
		endm

SCN_END		macro
		fcb $ff
		endm

		DATA

scn_title
		fdb scn_options,scn_controls
		fci 1,1,"BLOCKDOWN",0,-1
		fci 3,1,"(C) 2021 TEIPEN MWNCI",0,-1
		fci 15,0,"<< OPTIONS",0,21,"CONTROLS >>",0,-1
		SCN_CALL scn_ll_common
		SCN_END

scn_controls
		fdb scn_title,scn_scores0
		fci 0,24,"CONTROLS",0,-1
		fci 15,0,"<< TITLE",0,18,"LEADERBOARD >>",0,-1
		fci 2,12,"PLAYER 1",0,23,"PLAYER 2",0,-1
		fci 3,2,"MOVE L,R",0,15,"Z,C",0,26,"M,>",0,-1
		fci 4,2,"TURN ACW",0,16,"S",0,27,"K",0,-1
		fci 5,1,"SOFT DROP",0,16,"X",0,27,"<",0,-1
		fci 6,1,"HARD DROP",0,16,"3",0,27,"9",0,-1
		fci 7,6,"HOLD",0,16,"2",0,27,"8",0,-1
		fci 8,3,"TURN CW",0,16,"1",0,27,"7",0,-1
		fci 9,-1
		fci 10,6,"PAUSE",0,15,"CLEAR+ENTER",0,-1
		fci 11,7,"QUIT",0,15,"CLEAR+BREAK",0,-1
		SCN_CALL scn_ll_common
		SCN_END

scn_scores0
		fdb scn_controls,scn_scores1
		SCN_CALL scn_ldrbrd
		fci 15,0,"<< CONTROLS",0,18,"LEADERBOARD >>",0,-1
		fci 2,0,"NORMAL GAME",0,-1
		SCN_CALL scn_ll_common
		SCN_JSR draw_scores0
		SCN_END

scn_ldrbrd	fci 0,21,"LEADERBOARD",0,-1
		SCN_END

scn_scores1
		fdb scn_scores0,scn_options
		SCN_CALL scn_ldrbrd
		fci 15,0,"<< LEADERBOARD",0,22,"OPTIONS >>",0,-1
		fci 2,0,"FAST GAME",0,-1
		SCN_CALL scn_ll_common
		SCN_JSR draw_scores1
		SCN_END

; 2-player Game Over screen
scn_game_over_2
		fdb scn_title,scn_scores0
		fci 0,23,"GAME OVER",0,-1
		fci 15,0,"<< TITLE",0,18,"LEADERBOARD >>",0,-1
		fci 4,0,"PLAYER 1",0,-1
		fci 6,0,"PLAYER 2",0,-1
		SCN_JSR draw_2player_scores
		SCN_END

scn_options
		fdb scn_scores1,scn_title
		fci 0,25,"OPTIONS",0,-1
		fci 15,0,"<< LEADERBOARD",0,24,"TITLE >>",0,-1
		fci 3,6,"<P>",0,12,"PLAYERS:",0,-1
		fci 4,6,"<S>",0,14,"SPEED:",0,-1
		fci 5,6,"<G>",0,12,"GARBAGE:",0,-1
		SCN_CALL scn_ll_common
		SCN_JSR draw_options
		SCN_END

scn_ll_common	fci 13,5,"PRESS <SPACE> TO START",0,-1
		SCN_END

scn_lb_text	fci 13,1,"A HIGH SCORE - ENTER YOUR NAME",0,-1
		SCN_END

t_plrs_1	fci "1",0
t_plrs_2	fci "2",0

t_gspd_normal	fci "NORMAL",0
t_gspd_fast	fci "FAST  ",0

t_grb_easy	fci "EASY",0
t_grb_hard	fci "HARD",0

		section "DP"

; Ok to shadow in-game variables in the title page...

ttl_tmp0	equ tmp0
ttl_tmp1	equ tmp1
ttl_tmp2	equ tmp2
ttl_tmp3	equ tmp3

ttl_curscene	rmb 2

		CODE

; Entry:
; 	U -> new scene
; Exit:
; 	U preserved, points to scene jump table

draw_line_list
10		lda ,u+
		bpl 20F
		inca		; == $ff?
		bne >		; no? special command
		rts
!		inca		; == $fe?
		beq 12F		; yes? CALL
		; else JSR
		jsr [,u++]
		bra 10B
		; CALL
12		pulu x
		pshs u
		leau ,x
		bsr draw_line_list
		puls u
		bra 10B
20		sta vdg_y
		ldb #$20
		jsr [vdg_t_clr_line]
30		lda ,u+
		bmi 10B
		sta vdg_x
		leax ,u
		jsr [vdg_t_putstr]
		leau ,x
		bra 30B

draw_scene	cmpu ttl_curscene
		beq 10F
draw_scene_f	stu ttl_curscene
		clr ttl_tmp0
		clr ttl_tmp0+1
		lda reg_pia0_pdrb
		sync
		jsr [vdg_t_cls]
		ldu ttl_curscene
		leau 4,u	; skip left,right
		bsr draw_line_list
10		ldu ttl_curscene
		rts

opt_font	jsr vdg_cycle_font
		ldu ttl_curscene
		bra scene_loop_f

; start.s transfers control here after initialisation

main		jsr vdg_set_font
show_title	ldu #scn_title

scene_loop_f	clr ttl_curscene	; flag to redraw
scene_loop	bsr draw_scene

waitkey		jsr rnd_generate
		jsr kbd_scan
		beq run_timer
		clr ttl_tmp0	; reset timer
		clr ttl_tmp0+1	; on any keypress
		cmpa #32
		lbeq start_game
		cmpa #3
		beq show_title
		cmpa #6		; clear+f
		beq opt_font
		cmpa #8
		bne 10F
		ldu ,u		; "left" scene
		bra scene_loop
10		cmpa #9
		bne 20F
		ldu 2,u		; "right" scene
		bra scene_loop
20		cmpa #'1'
		beq opt_1player
		cmpa #'2'
		beq opt_2player
		cmpa #'P'
		beq opt_players
		cmpa #'N'
		beq opt_slow
		cmpa #'F'
		beq opt_fast
		cmpa #'S'
		beq opt_speed
		cmpa #'E'
		beq opt_easy_garbage
		cmpa #'H'
		beq opt_hard_garbage
		cmpa #'G'
		beq opt_garbage
30

run_timer
		lda reg_pia0_pdrb
		sync
		lda ttl_tmp0+1
		inca
		cmpa gravity_curve	; will be 50 or 60!
		blo 50F
		lda ttl_tmp0
		inca
		cmpa #30		; 30s between scenes
		blo 40F
		ldu 2,u
		bra scene_loop	; scene redraw will reset timer
40		sta ttl_tmp0
		clra
50		sta ttl_tmp0+1

		bra waitkey

opt_easy_garbage
		clr garbage_style
		bra 20F
opt_hard_garbage
		clr garbage_style
		; fall through to opt_garbage
opt_garbage	com garbage_style
		bra 20F

opt_1player	clr nplayers
		bra 20F
opt_2player	clr nplayers
		; fall through to opt_players
opt_players	com nplayers
20		ldu #scn_options
		jsr draw_scene
		bsr draw_options	; might end up drawing twice
		jmp scene_loop

opt_speed	lda game_speed
		eora #$0f
		fcb $8c		; cmpx nop 2 bytes
opt_fast	lda #5
		fcb $8c		; cmpx nop 2 bytes
opt_slow	lda #10
!		sta game_speed
		bra 20B

draw_options

		ldd #$0315
		std vdg_y

		ldx #t_plrs_1
		lda nplayers
		beq 10F
		ldx #t_plrs_2
10		bsr _t_putstr

		inc vdg_y
		ldx #t_gspd_normal
		lda game_speed
		cmpa #10
		beq 10F
		ldx #t_gspd_fast
10		bsr _t_putstr

		inc vdg_y
		ldx #t_grb_easy
		lda garbage_style
		beq 10F
		ldx #t_grb_hard
_t_putstr
10		jmp [vdg_t_putstr]

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DEFS"

sizeof_lbe_name	equ 13

		org 0
lbe_name	rmb sizeof_lbe_name
lbe_score	rmb 4
lbe_e_lvl	rmb 1
sizeof_lbe

num_lbe		equ 8

		BSS

highscores_normal
		rmb num_lbe*sizeof_lbe
highscores_normal_end

highscores_fast
		rmb num_lbe*sizeof_lbe
highscores_fast_end

		CODE

draw_scores1	ldx #highscores_fast
		bra >
draw_scores0	ldx #highscores_normal
!		ldb #num_lbe
		stb ttl_tmp0
		lda #$04
10		sta ttl_tmp0+1
		clrb
		jsr [vdg_t_yx_to_y]

		ldb #sizeof_lbe_name
!		lda ,x+
		jsr [vdg_t_putchr_y]
		decb
		bne <

		leay 2,y
		pshs y
		ldb #13
!		lda #$20
		jsr [vdg_t_putchr_y]
		decb
		bne <
		leay 2,y
		lda #$20
		jsr [vdg_t_putchr_y]
		lda #$20
		jsr [vdg_t_putchr_y]
		puls y

		leay 5,y
		ldb #8
		jsr [vdg_t_putbcd]
		leay 2,y
		ldb #2
		jsr [vdg_t_putbcd]

		lda ttl_tmp0+1
		inca
		dec ttl_tmp0
		bne 10B

		ldd #$021c
		ldx #vrate_text
		jmp [vdg_t_putstr_yx]

draw_2player_scores
		ldd #$0414
		jsr [vdg_t_yx_to_y]
		ldx #plr1+plr_score
		ldb #8
		jsr [vdg_t_putbcd]
		leay 2,y
		ldx #plr1+plr_level
		ldb #2
		jsr [vdg_t_putbcd]

		ldd #$0614
		jsr [vdg_t_yx_to_y]
		ldx #plr2+plr_score
		ldb #8
		jsr [vdg_t_putbcd]
		leay 2,y
		ldx #plr2+plr_level
		ldb #2
		jmp [vdg_t_putbcd]

ttl_game_over	lds #stack_top		; reset stack
		jsr [vdg_t_cls]

		; levels in-game are binary, but now we only care about
		; the BCD
		lda plr1+plr_level
		jsr bin8_to_bcd8
		sta plr1+plr_level
		lda plr2+plr_level
		jsr bin8_to_bcd8
		sta plr2+plr_level

		lda nplayers
		beq ttl_game_over_1

		; 2-player game over screen
		ldu #scn_game_over_2
		jmp scene_loop_f

		; 1-player game over screen
ttl_game_over_1

		ldu #highscores_normal
		ldy #scn_scores0
		lda game_speed
		cmpa #10
		beq >
		ldu #highscores_fast
		ldy #scn_scores1
!		sty ttl_curscene

		leax num_lbe*sizeof_lbe,u

		ldb #num_lbe		; number of entries to test against
		stb ttl_tmp3
10		ldd plr1+plr_score
		cmpd lbe_score,u
		bhi 20F
		blo >
		ldd plr1+plr_score+2
		cmpd lbe_score+2,u
		bhi 20F
!		leau sizeof_lbe,u
		dec ttl_tmp3
		bne 10B
		; didn't find a high score - oh well!
		leau ,y
		jmp scene_loop_f

		; ok, found a high score at position U
20		stu ttl_tmp1		; start of name address
!		lda -(sizeof_lbe+1),x
		sta ,-x
		cmpx ttl_tmp1
		bhi <
		ldd #$2000|sizeof_lbe_name
!		sta ,x+
		decb
		bne <
		stx ttl_tmp2		; end of name address
		ldd plr1+plr_score
		std ,x++
		ldd plr1+plr_score+2
		std ,x++
		lda plr1+plr_level
		sta ,x+

		leau 4,y
		jsr draw_line_list
		ldu #scn_lb_text
		jsr draw_line_list
		ldu ttl_tmp1

		lda #num_lbe+4
		suba ttl_tmp3
		clrb
		jsr [vdg_t_yx_to_y]

10		clr ttl_tmp0
20		com ttl_tmp0
		lda delay_data	; consistent flash rate!
		sta ttl_tmp0+1
		lda ttl_tmp0
		bne 24F
		lda #$20
		cmpu ttl_tmp2
		blo 26F
		lda #$80
		fcb $8c		; cmpx nop 2 bytes
24		lda #$cf
26		jsr [vdg_t_putchr_y]
		leay -1,y
30		jsr rnd_generate
		lda reg_pia0_pdrb
40		jsr kbd_scan
		bne 60F
		lda reg_pia0_crb
		bpl 40B
		dec ttl_tmp0+1
		bne 30B
		bra 20B
50		ldu ttl_curscene
		jmp scene_loop_f

60		cmpa #13
		beq 50B
		cmpa #8
		bne 70F
		cmpu ttl_tmp1
		beq 30B
		ldd #$2020
		cmpu ttl_tmp2
		blo 65F
		lda #$80
		fcb $8c		; cmpx nop 2 bytes
65		lda #$20
		stb ,-u
		jsr [vdg_t_putchr_y]
		leay -2,y
		bra 10B
70		cmpa #$20
		blo 10B
		cmpa #$7f
		bhs 10B
		cmpu ttl_tmp2
		beq 10B
		cmpa #$40
		blo 75F
		eora #$40
75		sta ,u+
		jsr [vdg_t_putchr_y]
		bra 10B

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DP"

ttl_bcd		rmb 1

		CODE

; dumb little converter

bin8_to_bcd8	pshs b
		clrb
10		cmpa #10
		blo 20F
		addb #$10
		suba #10
		bra 10B
20		pshs b
		adda ,s+
		puls b,pc
