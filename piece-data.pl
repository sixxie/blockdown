#!/usr/bin/perl -wT
use strict;

my @colours = (
	'green', 'yellow', 'blue', 'red',
	'white', 'cyan', 'magenta', 'orange'
);

my %colour_by_name = ();
@colour_by_name{@colours} = (0..$#colours);

my %pieces = (

	i => {
		colour => 'cyan',
		bitmaps => [
			[
				"    ",
				"####",
				"    ",
				"    ",
			],
			[
				"  # ",
				"  # ",
				"  # ",
				"  # ",
			],
			[
				"    ",
				"    ",
				"####",
				"    ",
			],
			[
				" #  ",
				" #  ",
				" #  ",
				" #  ",
			],
		],
		rotations => 'i',
	},

	j => {
		colour => 'blue',
		bitmaps => [
			[
				"#   ",
				"### ",
				"    ",
				"    ",
			],
			[
				" ## ",
				" #  ",
				" #  ",
				"    ",
			],
			[
				"    ",
				"### ",
				"  # ",
				"    ",
			],
			[
				" #  ",
				" #  ",
				"##  ",
				"    ",
			],
		],
		rotations => 'jlstz',
	},

	l => {
		colour => 'orange',
		bitmaps => [
			[
				"  # ",
				"### ",
				"    ",
				"    ",
			],
			[
				" #  ",
				" #  ",
				" ## ",
				"    ",
			],
			[
				"    ",
				"### ",
				"#   ",
				"    ",
			],
			[
				"##  ",
				" #  ",
				" #  ",
				"    ",
			],
		],
		rotations => 'jlstz',
	},

	o => {
		colour => 'yellow',
		bitmaps => [
			[
				" ## ",
				" ## ",
				"    ",
				"    ",
			],
			[
				" ## ",
				" ## ",
				"    ",
				"    ",
			],
			[
				" ## ",
				" ## ",
				"    ",
				"    ",
			],
			[
				" ## ",
				" ## ",
				"    ",
				"    ",
			],
		],
		rotations => 'o',
	},

	s => {
		colour => 'green',
		bitmaps => [
			[
				" ## ",
				"##  ",
				"    ",
				"    ",
			],
			[
				" #  ",
				" ## ",
				"  # ",
				"    ",
			],
			[
				"    ",
				" ## ",
				"##  ",
				"    ",
			],
			[
				"#   ",
				"##  ",
				" #  ",
				"    ",
			],
		],
		rotations => 'jlstz',
	},

	t => {
		colour => 'magenta',
		bitmaps => [
			[
				" #  ",
				"### ",
				"    ",
				"    ",
			],
			[
				" #  ",
				" ## ",
				" #  ",
				"    ",
			],
			[
				"    ",
				"### ",
				" #  ",
				"    ",
			],
			[
				" #  ",
				"##  ",
				" #  ",
				"    ",
			],
		],
		rotations => 'jlstz',
	},

	z => {
		colour => 'red',
		bitmaps => [
			[
				"##  ",
				" ## ",
				"    ",
				"    ",
			],
			[
				"  # ",
				" ## ",
				" #  ",
				"    ",
			],
			[
				"    ",
				"##  ",
				" ## ",
				"    ",
			],
			[
				" #  ",
				"##  ",
				"#   ",
				"    ",
			],
		],
		rotations => 'jlstz',
	},

);

# Rotations are in order first by initial rotation (0,R,2,L), then by rotation
# direction (right, left).

my %rotations = (

	jlstz => [
		[ # from 0
			[ [ 0, 0], [ 1, 0], [ 1, 1], [ 0,-2], [ 1,-2] ], # 0->L
			[ [ 0, 0], [-1, 0], [-1, 1], [ 0,-2], [-1,-2] ], # 0->R
		],
		[ # from R
			[ [ 0, 0], [ 1, 0], [ 1,-1], [ 0, 2], [ 1, 2] ], # R->0
			[ [ 0, 0], [ 1, 0], [ 1,-1], [ 0, 2], [ 1, 2] ], # R->2
		],
		[ # from 2
			[ [ 0, 0], [-1, 0], [-1, 1], [ 0,-2], [-1,-2] ], # 2->R
			[ [ 0, 0], [ 1, 0], [ 1, 1], [ 0,-2], [ 1,-2] ], # 2->L
		],
		[ # from L
			[ [ 0, 0], [-1, 0], [-1,-1], [ 0, 2], [-1, 2] ], # L->2
			[ [ 0, 0], [-1, 0], [-1,-1], [ 0, 2], [-1, 2] ], # L->0
		],
	],

	i => [
		[ # from 0
			[ [ 0, 0], [-1, 0], [ 2, 0], [-1, 2], [ 2,-1] ], # 0->L
			[ [ 0, 0], [-2, 0], [ 1, 0], [-2,-1], [ 1, 2] ], # 0->R
		],
		[ # from R
			[ [ 0, 0], [ 2, 0], [-1, 0], [ 2, 1], [-1,-2] ], # R->0
			[ [ 0, 0], [-1, 0], [ 2, 0], [-1, 2], [ 2,-1] ], # R->2
		],
		[ # from 2
			[ [ 0, 0], [ 1, 0], [-2, 0], [ 1,-2], [-2, 1] ], # 2->R
			[ [ 0, 0], [ 2, 0], [-1, 0], [ 2, 1], [-1,-2] ], # 2->L
		],
		[ # from L
			[ [ 0, 0], [-2, 0], [ 1, 0], [-2,-1], [ 1, 2] ], # L->2
			[ [ 0, 0], [ 1, 0], [-2, 0], [ 1,-2], [-2, 1] ], # L->0
		],
	],


	o => [
		[ # from 0
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # 0->L
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # 0->R
		],
		[ # from R
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # R->0
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # R->2
		],
		[ # from 2
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # 2->R
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # 2->L
		],
		[ # from L
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # L->2
			[ [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0], [ 0, 0] ], # L->0
		],
	],

);

my @rotation_names = ( '0', '1', '2', '3' );

# Convert ASCII-art into bitmaps
for my $piece (sort keys %pieces) {
	my $p = $pieces{$piece};
	my $bitmaps = $p->{bitmaps};
	my $no = scalar(@{$bitmaps});
	for my $o (0..$no-1) {
		for my $j (0..3) {
			my $s = $bitmaps->[$o]->[$j];
			my $v = 0;
			for my $i (0..3) {
				$v <<= 1;
				$v |= 1 if (substr($s, $i, 1) eq '#');
			}
			$bitmaps->[$o]->[$j] = $v;
		}
	}
}

# Piece definitions

print <<__EOF__;
; Draw data starts with a word describing offset in screen bytes of first
; block (which may be negative), then a byte positive offset for the three
; subsequent blocks.
;
; Stack data is broadly similar.  Only one byte is necessary for the
; initial offset, and all offsets are relative to the top-left.
__EOF__

for my $piece (sort keys %pieces) {
	my $p = $pieces{$piece};
	my $colour = $colour_by_name{$p->{colour}};
	my $sg_colour = 0x80 | ($colour << 4);
	my $block_colour = $sg_colour | 0x0f;
	my @preview = ();
	push @preview, bitmap_to_sg($sg_colour, $p->{bitmaps}->[0]->[0] >> 2);
	push @preview, bitmap_to_sg($sg_colour, $p->{bitmaps}->[0]->[0]);
	push @preview, bitmap_to_sg($sg_colour, $p->{bitmaps}->[0]->[1] >> 2);
	push @preview, bitmap_to_sg($sg_colour, $p->{bitmaps}->[0]->[1]);
	my $rot = $p->{rotations};

	print "\npc_${piece}\n";
	#print "\t\tfdb pc_${rot}_rotations\t; ptr to rotation data\n";
	printf "\t\tfcb \$\%02x\t\t\t; colour = \%s\n", $block_colour, $p->{colour};
	print "\t\tfcb ".join(",", map { sprintf "\$\%02x", $_ } @preview);
	print "\t; next preview\n";

	my $bitmaps = $p->{bitmaps};
	my $no = scalar(@{$bitmaps});
	for my $o (0..$no-1) {
		print "\npc_${piece}_odata_${o}\n";

		my $obitmap = $bitmaps->[$o];
		print "\t\t; draw data, piece ${piece} orientation $rotation_names[$o]\n";
		my $loff;
		for my $y (0..3) {
			my $v = $obitmap->[$y];
			for my $x (0..3) {
				next if (($v & (1 << (3-$x))) == 0);
				my $soff = ($y * 128) + $x;
				if (!defined $loff) {
					printf "\t\tfdb \%d\n", $soff;
				} else {
					printf "\t\tfcb \$\%02x\n", $soff - $loff;
				}
				$loff = $soff;
			}
		}

		# Same again, but for stack...  Not relative this time though
		print "\n";
		print "\t\t; stack data, piece ${piece} orientation $rotation_names[$o]\n";
		for my $y (0..3) {
			my $v = $obitmap->[$y];
			for my $x (0..3) {
				next if (($v & (1 << (3-$x))) == 0);
				my $soff = ($y * 13) + $x;
				if (!defined $loff) {
					printf "\t\tfcb \%d\n", $soff;
				} else {
					printf "\t\tfcb \$\%02x\n", $soff;
				}
				$loff = $soff;
			}
		}

		my $oleft = ($o-1) % $no;
		my $oright = ($o+1) % $no;

		print "\n";
		my $r = $rotations{$rot};
		my $kl = $r->[$o];
		my $sn = $rotation_names[$o];
		#my @onames = ($oleft,$oright);
		for my $d (0..1) {
			my $odest = ('acw', 'cw')[$d];
			print "pc_${piece}_rot_${o}_${odest}\tfcb ";
			my $dn = $rotation_names[($o+($d*2)-1)%4];
			my $dkl = $kl->[$d];
			my @pairs = @{$dkl};
			shift @pairs;  # first is always 0,0
			my @vals = ();
			while (my $pair = shift @pairs) {
				push @vals, encode_xyoff(@{$pair});
			}
			print join(",", map { sprintf("\$\%02x", $_) } @vals);
			print "\t; ${sn}->${dn}\n";
		}

		print "\n";
		print "\t\tfdb pc_${piece}_odata_${oleft}\t; rotate left odata\n";
		print "\t\tfdb pc_${piece}_odata_${oright}\t; rotate right odata\n";

	}
}

sub bitmap_to_sg {
	my ($sg_colour,$bits) = @_;
	my $v = 0;
	$v |= 0x0a if ($bits & 0x02);
	$v |= 0x05 if ($bits & 0x01);
	return $sg_colour | $v;
}

sub encode_xyoff {
	my ($xo,$yo) = @_;
	my $r = 0;

	if ($xo == -2) {
		$r = 0xc0;
	} elsif ($xo == -1) {
		$r = 0x80;
	} elsif ($xo == 1) {
		$r = 0x40;
	} elsif ($xo == 2) {
		$r = 0x60;
	}

	# positive values for us mean go _down_
	$yo = -$yo;
	if ($yo == -2) {
		$r = ($r >> 2) | 0xc0;
	} elsif ($yo == -1) {
		$r = ($r >> 2) | 0x80;
	} elsif ($yo == 1) {
		$r = ($r >> 3) | 0x40;
	} elsif ($yo == 2) {
		$r = ($r >> 3) | 0x60;
	} else {
		$r = ($r >> 2);
	}
	return $r;
}
