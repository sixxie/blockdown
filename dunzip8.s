		section "DP"

duz8_end	rmb 2

; dunzip8 - unzip very simply coded zip data

; 1iiiiiii nnnnnnnn - repeat 128-n (1-256 bytes from current + 1i (-ve 2s cmpl)
; 0nnnnnnn - directly copy next 128-n (1-128) bytes

; entry: x = zip start, d = zip end, u = destination

		CODE

dunzip8		std duz8_end
duz8_loop	ldd ,x++
		bpl duz8_run	; run of 1-128 bytes
duz8_7_8	leay a,u	; copy 1-128 bytes
10		lda ,y+
		sta ,u+
		incb
		bvc 10B		; count UP until B == 128
		bra 80F
20		ldb ,x+
duz8_run	stb ,u+
		inca
		bvc 20B		; count UP until B == 128
80		cmpx duz8_end
		blo duz8_loop
		rts
