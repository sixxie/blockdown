; Video mode and text printing.

		section "DEFS"

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DP"

vdg_tmp0	rmb 1
vdg_tmp1	rmb 1
vdg_y		rmb 1
vdg_x		rmb 1
vdg_font	rmb 2		; or 0 for 'vdg' font
vdg_mode	rmb 1		; $08 (VDG) or $38 (T1)
vdg_eor		rmb 1		; $00 (VDG) or $40 (T1)

; Font configuration:
vdg_nfonts	rmb 1		; number of fonts (ignored for CoCo 3)
vdg_sel_font	rmb 1		; currently selected font index

		BSS

; Initialisation (start.s) detects machine and will populate this set of
; data and function pointers.

vdg_per_machine

; Title functions:
vdg_t_yx_to_y	rmb 2		; convert Y,X in A,B to screen addr in Y
vdg_t_cls	rmb 2		; ptr to set & clear title video screen
vdg_t_clr_line	rmb 2		; ptr to single-line clear
vdg_t_putstr_yx	rmb 2		; ptr to put string at specific Y,X
vdg_t_putstr	rmb 2		; ptr to put string at current Y,X
vdg_t_putchr_y	rmb 2		; ptr to put single character at Y
vdg_t_putbcd	rmb 2		; print B bytes of BCD from Y to X

; In-game functions:
vdg_g_cls	rmb 2		; ptr to set & clear game video screen
vdg_g_putdigit	rmb 2		; ptr to in-game digit draw
vdg_g_putbcd	rmb 2		; as vdg_t_putbcd but in-game

; Note that vdg_sel_font is an index; it is used to update vdg_font and
; vdg_eor by code in title.s.

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		DATA

font_askew	include "font-askew.s"
font_askew_t1	include "font-askew-t1.s"
font_cc3_num	include "font-cc3-num.s"

playscreen_1up	includebin "playscreen-1up.bin.dz8"
playscreen_1up_end
playscreen_2up	includebin "playscreen-2up.bin.dz8"
playscreen_2up_end

t_stats		fci "LVL",0,"   ",0,"LNS",0,"   ",0,0

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; CoCo 3 note: Semigraphics is basically broken on the CoCo 3, but these
; values allow the graphics characters to be shown, even though the
; alphanumeric characters will not be.  Store in $FF9C:

; Mode	Value
; SG4	15
; SG6	7
; SG8	8
; SG12	9
; SG24	10

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

;;; Title text routines

		DATA

vdg_fontspecs	; VDG
		fdb $0000
		fdb $0800
		; Askew
		fdb font_askew
		fdb $0800
		; Askew T1
		fdb font_askew_t1
		fdb $3840

		CODE

; Cycle to next font.

vdg_cycle_font	ldb vdg_sel_font
		incb
		; fall through to vdg_set_font

; Select font.

; Entry:
; 	B = font number (0 = VDG, 1 = Askew, 2 = Askew T1)

		fcb $8c		; cmpx nop 2 bytes
vdg_set_font	ldb vdg_sel_font
		pshs x
		cmpb vdg_nfonts
		blo 10F
		clrb
10		stb vdg_sel_font
		ldx #vdg_fontspecs
		lslb
		lslb
		abx
		ldd ,x
		std vdg_font
		ldd 2,x
		std vdg_mode	; and vdg_eor
		puls x,pc

; Setup title video mode and clear screen.

coco3_t_cls
		sta reg_sam_v2c
		lda #15
		sta $ff9c
		; fall through to dragon_t_cls

dragon_t_cls
		; VDG mode 0, CSS1
		lda vdg_mode
		sta reg_pia1_pdrb

		; For the CoCo 3, this is actually clearing more memory
		; than we need to, but doesn't really matter.
		ldx #fb0
		ldd #$8080
!		std ,x++
		cmpx #fb0_end
		blo <

		rts

; Clears a single line.

; Entry:
; 	A = line
; 	B = value

coco3_t_clr_line
		pshs b
		clrb
		jsr coco3_t_yx_to_y
		lda #32
		bra 10F

dragon_t_clr_line
		eorb vdg_eor
		pshs b
		clrb
		jsr dragon_t_yx_to_y
		lda #192
10		puls b
!		stb ,y+
		deca
		bne <
		rts

; Setup game video mode and clear screen.

coco3_g_cls
		sta reg_sam_v2s
		lda #9
		sta $ff9c
		; fall through to dragon_g_cls

dragon_g_cls
		; VDG mode 0, CSS1
		lda vdg_mode
		sta reg_pia1_pdrb

		; Unzip appropriate playscreen
		lda nplayers
		beq >
		ldx #playscreen_2up
		ldd #playscreen_2up_end
		bra 10F
!		ldx #playscreen_1up
		ldd #playscreen_1up_end
10		ldu #fb0
		jsr dunzip8
		lda nplayers
		beq >
		ldd #$0b0d
		bsr put_stats_text
		ldd #$0b1d
		bra put_stats_text
!		ldd #$0b15
		; fall through to put_stats_text

; After unpacking common game screen data, need to draw font-specific text
; in-place, potentially twice at different X offsets.  Always uses Dragon
; putstr, as we're always in that video mode, and the results on a CoCo 3
; will just be invisible.
;
; Entry:
; 	A = Y position
; 	B = X position

put_stats_text
		std vdg_y
		ldx #t_stats
!		bsr dragon_t_putstr
		inc vdg_y
		lda ,x
		bne <
		rts

dragon_t_yx_to_y
		pshs a,b
10		ldb #192
		mul
		addb 1,s
		ldy #fb0
		leay d,y
		puls a,b,pc

; Set Y,X and put string.  No effort is made to wrap text.

; Entry:
; 	A = Y coordinate
; 	B = X coordinate
; 	X -> nul-terminated string

dragon_t_putstr_yx
		std vdg_y
		; fall through to dragon_t_putstr

; Put string to current Y,X.

; Entry:
; 	X -> nul-terminated string

dragon_t_putstr
		ldd vdg_y
		bsr dragon_t_yx_to_y

dragon_g_putstr_y
10		lda ,x+
		bne 20F
		rts
20		bsr dragon_t_putchr_y
		bra 10B

; Put single character to Y register.
;
; Exit;
; 	A destroyed
; 	Y incremented
; 	B, X preserved

dragon_t_putchr_y
30		pshs x

		; font-based character (alpha, @[\]^_)?
		cmpa #$20
		blo 40F		; yes?  draw from font
		; no? draw as-is
30		eora vdg_eor
		sta 160,y
		sta 128,y
		sta 96,y
		sta 64,y
		sta 32,y
		sta ,y+
		puls x,pc

		; draw character from font
40		ldx vdg_font
		beq 30B		; no font?  go back and draw as-is!
		pshs b
		ldb #6
		mul
		leax d,x
		ldd 4,x
		stb 160,y
		sta 128,y
		ldd 2,x
		stb 96,y
		sta 64,y
		ldd ,x
		stb 32,y
		sta ,y+
		puls b,x,pc

coco3_t_yx_to_y
		pshs a,b
10		ldb #32
		mul
		addb 1,s
		ldy #fb0
		leay d,y
		puls a,b,pc

; CoCo 3 version of putstr is much simpler as we're just using bog
; standard SemiGraphics 4 mode.

coco3_t_putstr_yx
		std vdg_y
		; fall through to coco3_t_putstr

coco3_t_putstr
		ldd vdg_y
		bsr coco3_t_yx_to_y

coco3_t_putstr_y
10		lda ,x+
		bne 20F
		rts
20		sta ,y+		; bsr coco3_t_putchr_y
		bra 10B

coco3_t_putchr_y
		sta ,y+
		rts

; Draw digit for lower 4 bits of A to Y.

dragon_g_putdigit
		anda #$0f
		adda #$30
		eora vdg_eor
		sta 160,y
		sta 128,y
		sta 96,y
		sta 64,y
		sta 32,y
		sta ,y+
		rts

; Bit more contrived for CoCo 3 - a really low-res font.  Different
; colours in different columns so you can see where one digit ends and the
; next begins!

coco3_g_putdigit
		pshs a,b,x
		tfr y,d
		lslb
		lslb
		lslb
		lslb
		stb vdg_tmp0
		puls a
		anda #$0f
		ldx #font_cc3_num
		ldb #5
		mul
		leax d,x
		lda 4,x
		ora vdg_tmp0
		sta 128,y
		ldd 2,x
		ora vdg_tmp0
		orb vdg_tmp0
		sta 64,y
		stb 96,y
		ldd ,x
		ora vdg_tmp0
		orb vdg_tmp0
		stb 32,y
		sta ,y+
		puls b,x,pc

; Entry:
; 	Y -> screen
; 	X -> BCD sequence
; 	B -> number of digits

; This version is generalised to use putdigit so that it can be used by
; the CoCo 3 in-game too.

coco3_g_putbcd
dragon_g_putbcd
dragon_t_putbcd
		lda #$10
		sta vdg_tmp1
		bitb #1
		bne 35F
		decb
10		lda ,x
		lsra
		lsra
		lsra
		lsra
		beq 20F
		clr vdg_tmp1
		bra 30F
20		lda vdg_tmp1
		beq 30F
		leay 1,y
		bra 35F
30		jsr [vdg_g_putdigit]
35		decb
		bne 40F
		clr vdg_tmp1
40		lda ,x+
		anda #$0f
		beq 50F
		clr vdg_tmp1
		bra 60F
50		lda vdg_tmp1
		beq 60F
		leay 1,y
		bra 65F
60		jsr [vdg_g_putdigit]
65		decb
		bpl 10B
		rts

; CoCo 3 version of putbcd.

coco3_t_putbcd
		lda #$10
		sta vdg_tmp1
		bitb #1
		bne 35F
		decb
10		lda ,x
		lsra
		lsra
		lsra
		lsra
		beq 20F
		clr vdg_tmp1
		bra 30F
20		lda vdg_tmp1
30		eora #$30
		sta ,y+
35		decb
		bne 40F
		clr vdg_tmp1
40		lda ,x+
		anda #$0f
		beq 50F
		clr vdg_tmp1
		bra 60F
50		lda vdg_tmp1
60		eora #$30
		sta ,y+
		decb
		bpl 10B
		rts
