		section "DEFS"

; Player data.  Most of it is predictably about the current piece: its
; screen address for drawing, its position within in the in-memory stack,
; and which bitmap currently represents its horizontal position for
; testing.

; Also:
; - base address of the player's window on screen
; - the currently 'held' piece
; - the player's stack
; - random bag
; - score

; And a pointer to the other player - at some point we're going to want
; them to be able to send each other garbage!

; Ghost piece is tracked whether visible or not.  Means that downward
; movement is basically checked for us: equal to host piece address?  Then
; it can't move!

sizeof_score	equ 4
sizeof_nlines	equ 2
max_garb	equ 12

; ========================================================================
;;; Player structure

plr_org_	org 0-plr_offset_

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;;; 16 bytes to try and keep local as they are used frequently.

plr_scrn	rmb 2		; screen base for player
plr_stck	rmb 2		; player stack base address
plr_pc_scrn	rmb 2		; screen address for current piece
plr_pc_stck	rmb 2		; pointer into stack
plr_pc_odata	rmb 2		; current orientation data
plr_pc_held	rmb 2		; currently 'held' piece (or null)
plr_pc_colr	rmb 1		; piece colour
plr_state	rmb 1		; game state - see below

; Most of the game logic happens during VSYNC, but the screen updates
; outside the play area don't have to.  These flags are used to indicate
; something needs updated during a non-critical time.

plr_f_drawheld	rmb 1		; flag draw held piece
plr_f_drawnext	rmb 1		; flag update next piece preview

plr_offset_	equ *-plr_org_

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
;;; Another 16 bytes to keep local for frequent use.

; Miscellaneous 'delay' counter.  Used differently in different states,
; e.g. to track lock delay, entry delay, gravity.  Single most frequently
; referenced variable, so it gets pride of place at zero offset, which is
; slightly quicker.

plr_delay	rmb 1		; lock, gravity, entry delay

plr_kbd_state	rmb 1		; keyboard state
plr_kbd_new	rmb 1		; newly-pressed keys
plr_kbd_mmask	rmb 1		; move mask
plr_kbd_ldas	rmb 1		; left delayed auto-shift
plr_kbd_rdas	rmb 1		; right delayed auto-shift
plr_kbd_srpt	rmb 1		; soft drop repeat
plr_lockreset	rmb 1		; lock reset counter (from lvl_lk_rst)
plr_level	rmb 1		; current level
plr_lvl_lines	rmb 1		; lines left in level (from game_speed)
plr_lvl_idas	rmb 1		; level initial DAS (precede rdas)
plr_lvl_rdas	rmb 1		; level repeat DAS (follow idas)

; Flag that the current piece is eligible for a T-spin bonus if it locks.

plr_tspin	rmb 1

; Ghost piece: where it is displayed on screen, and its address within the
; stack.  This is useful in many places, so it's kept up to date even if
; player has chosen not to display it (if indeed I add that option...).

plr_gh_scrn	rmb 2		; screen address for ghost piece
plr_gh_stck	rmb 2		; stack address for ghost piece

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
; The rest of the player structure doesn't need such quick access.

; Track back-to-back difficult clears.
plr_difficult	rmb 1

; Pointer to other player for sending garbage.
plr_other	rmb 2

; Garbage tracking.  The timer is only decremented during state_normal.
; The queued data area is 16 elements long - larger than it needs to be -
; so that indices can be easily masked.

plr_grb_send	rmb 1		; garbage to send this frame
plr_grb_recv	rmb 1		; garbage received from queue
plr_grb_timer	rmb 2		; time until next garbage dequeued
plr_grb_head	rmb 1		; head of queue
plr_grb_tail	rmb 1		; tail of queue
plr_grb_total	rmb 1		; total garbage in queue
plr_grb		rmb 16		; queue larger than it needs to be
plr_grb_offset	rmb 1		; number from -10..-1
plr_f_grb	rmb 1		; flag redraw of garbage queue
plr_f_scrn	rmb 1		; flag playfield redraw (stack raised)

; Player score, stored in BCD.  score_old is the previously-displayed
; value, used to track which digits need redrawn.

plr_score	rmb sizeof_score
plr_score_old	rmb sizeof_score

; Score accumulator for earned but not yet rewarded points.

plr_scoreacc	rmb 1

; The line clear scores are all multiples of 100, and need to be
; calculated based on level meaning they're not in BCD (not without large
; tables anyway).  These track adding the BCD value for each bit in the
; calculated bonus to the score in turn.

plr_score50_idx	rmb 1		; counter for adding 50s to score
plr_score50	rmb 2		; binary number of 50s to add

plr_score_colr	rmb 1		; colour to draw player's score

; Combo counter (no. of line clearing locks in a row)

plr_combo	rmb 1

; More score-like stats: number of lines cleared.  Stored in BCD, with an
; "old" counterpart, as with score.

plr_nlines	rmb sizeof_nlines
plr_nlines_old	rmb sizeof_nlines

; Piece queue.  First entry is the current piece in play, followed by six
; "next" pieces.  Each is just a pointer to a piece definition.

plr_pcq		rmb 7*2		; piece queue - first is current active

; Shadow some data for use during game over sequence.  We don't need the
; piece queue by that point...

plr_go_stck	equ plr_pcq
plr_go_scrn	equ plr_go_stck+2

; The player's "bag".  A list of all seven pieces, shuffled each time it
; is "emptied" (bag_npc reduced to 0).

plr_bag_npc	rmb 1		; pieces left in bag
plr_bag		rmb 7*2		; shuffled pointers to pieces

; Player is old allowed to "hold" their piece once per drop, this is set
; non-zero when they do.

plr_has_held	rmb 1

; Level data.  Could look this up each time, but might as well just record
; current values here each time level changes.  Note some level data is in
; the fast access section above.

plr_lvl_gdelay	rmb 1		; level gravity delay (1/G)
plr_lvl_glines	rmb 1		; level lines per drop (G > 1)
plr_lvl_entr	rmb 1		; level entry delay (precede entr_lc)
plr_lvl_entr_lc	rmb 1		; level entry delay (lclear) (follow entr)
plr_lvl_lk_dly	rmb 1		; level lock delay (precede lk_rst)
plr_lvl_lk_rst	rmb 1		; level max lock resets (follow lk_dly)

; Bit of a bodge.  Set to non-zero if the other player got game over!

plr_winner	rmb 1

sizeof_plr	equ *+plr_offset_

;;; End of player structure
; ========================================================================

;;; Game states

; Each state is a multiple of 4.  This is because I'm dicking with the
; jump table, and alternate 16-bit words are actually characters from an
; embedded secret string.

; First state - sets up initial previews, etc. then transfers to
; state_entry.

state_startup	equ 0

; The usual in-game state.  Controls are processed, gravity applied, etc.

state_normal	equ 2

; Enter this state when a piece locks.  After this next state will either
; be to do line clears or straight to entry delay.  Queue a lock chirp.

state_lock	equ 4

; Line clear state.  This does the animation.  Not quite decided yet, but
; something like:
; - Fill all completed lines with white, maybe a line per frame?
; - Clear those lines - can I think of a good animation?  Slide off?
; - Move screen data down accordingly
; - And the stack data
; - Move to entry delay

state_clear	equ 6

state_fall	equ 8

; A short delay before the next piece is in play.  Last frame should be
; testing for initial rotation.  Any press of left/right during this
; should skip the delay aspect of DAS.  Next state is either normal or
; gameover (block out).

state_entry	equ 10

; All over bar the screaming.

state_gameover	equ 12

; State to swap held piece and then move to state_entry.

state_hold	equ 14

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DP"

plr_tmp0	rmb 1		; only need 1 byte

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		BSS

plr1		equ *+plr_offset_
		rmb sizeof_plr
plr2		equ *+plr_offset_
		rmb sizeof_plr

plr1_stack	rmb 3+(stack_nlines+2)*stack_width
plr2_stack	rmb 3+(stack_nlines+2)*stack_width

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Per-level data.  Most of this is related to play speed, and so there are
; sets for both 50Hz and 60Hz machines.  Conversions is necessarily
; imprecise, so games played in 50Hz can't really be compared to those
; played in 60Hz, but we try and get it close-ish.
;
; And now all this data is in start.s, as it's copied into place based on
; refresh rate detection.

		BSS

gravity_curve	rmb 16*2	; either 50Hz or 60Hz curve from start.s
delay_data	rmb 9*6		; either 50Hz or 60Hz data from start.s
vrate_text	rmb 5		; enough space for "NTSC" + nul

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; Set up initial player variables.

init_player
		; zeroise everything first
		leay -plr_offset_,u
		ldb #sizeof_plr
!		clr ,y+
		decb
		bne <

		; passed in
		stx plr_stck,u

		; Initialise stack.  Left & right walls are shared in this
		; structure, and are 3 cells wide.  $0f in left wall
		; indicates this is a normal line, $ff indicates it's the
		; base (so stop looking for line clears).

		lda #stack_nlines
		sta plr_tmp0
10		ldd #$0f0f
		std ,x++		; left wall
		sta ,x+
		ldd #$800a
!		sta ,x+
		decb
		bne <
		dec plr_tmp0
		bne 10B
		; now 2 lines of base plus final right wall
		ldd #$ff1d
!		sta ,x+
		decb
		bne <

		ldd #$ff0f
		sta plr_score_old+sizeof_score-1,u	; flag to draw last bit of score
		stb plr_nlines_old+1,u	; flag to draw last digit

		; initialise bag
		leay plr_bag,u
		ldx #pc_i
		ldb #7
!		stx ,y++
		leax sizeof_pc,x
		decb
		bne <

		lda #$ff
		sta snd_c1sfx_dur
		sta snd_c2sfx_dur
		rts

; Initialise both players.

init_players
		; player 2
		ldu #plr2
		ldx #plr2_stack
		bsr init_player
		ldx #fb0+16
		stx plr_scrn,u
		; player 1
		ldu #plr1
		ldx #plr1_stack
		bsr init_player
		ldx #fb0+8		; default for single-player
		stx plr_scrn,u
		stu plr2+plr_other
		lda #$d0
		sta plr_score_colr,u
		; if 2-player, point player 1 at player 2
		lda nplayers
		beq 99F
		ldx #fb0		; override for 2-player
		stx plr1+plr_scrn
		ldu #plr2
		stu plr1+plr_other
		lda #$e0
		sta plr_score_colr,u
99		rts

update_level	ldx #gravity_curve
		lda plr_level,u
		cmpa #16
		blo 10F
		lda #15
10		lsla
		ldd a,x
		sta plr_lvl_gdelay,u
		stb plr_lvl_glines,u
		lda plr_lvl_lines,u
		adda game_speed
		sta plr_lvl_lines,u

		; Various level-based delays.  They don't actually vary
		; until after level 15 (20G), where they progress towards
		; what I think is basically unplayable.  Bet someone can
		; keep it going though...

		ldb plr_level,u
		subb #15
		bpl 10F
		clrb
10		cmpb #9
		blo 20F
		ldb #4
20		lda #6
		mul
		ldx #delay_data
		abx
		ldd ,x
		std plr_lvl_entr,u	; and plr_lvl_entr_lc
		ldd 2,x
		std plr_lvl_lk_dly,u	; and plr_lvl_lk_rst
		ldd 4,x
		std plr_lvl_idas,u	; and plr_lvl_rdas

		; print level
		ldx plr_scrn,u
		leay 72*32+14,x
		ldb plr_level,u
		clra
		; stupid binary to ascii
10		cmpb #10
		blo 20F
		subb #10
		inca
		bra 10B
20		tsta
		bne 25F
		leay 1,y
		bra 26F
25		jsr [vdg_g_putdigit]
26		tfr b,a
		jsr [vdg_g_putdigit]
99		rts
