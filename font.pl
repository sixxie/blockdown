#!/usr/bin/perl -wT
use strict;

use Getopt::Long;
Getopt::Long::Configure("gnu_getopt", "pass_through");

my $variant = "vdg";

GetOptions(
        "vdg" => sub { $variant = "vdg"; },
        "t1" => sub { $variant = "t1"; },
) or exit(2);

my @font = (
	[ "@@@@", "@@@@" ],	# '@'
	[ "AAHA", "AAHA" ],	# 'A'
	[ "CBBB", "CBBB" ],	# 'B'
	[ "CCCC", "CLCe" ],	# 'C'
	[ "CDDD", "CDDD" ],	# 'D'
	[ "CE2[", "CE2e" ],	# 'E'
	[ "CF2F", "CF2F" ],	# 'F'
	[ "GGGC", "GGGG" ],	# 'G'
	[ "LHHH", "LHHH" ],	# 'H'
	[ "!IIT", "!IIT" ],	# 'I'
	[ "!II;", "!II}" ],	# 'J'
	[ "LKKK", "LKKK" ],	# 'K'
	[ "LLL[", "LLLe" ],	# 'L'
	[ "LMMM", "LMMM" ],	# 'M'
	[ "LNNN", "LNNN" ],	# 'N'
	[ "COOC", "OOOO" ],	# 'O'
	[ "CPPP", "CPPP" ],	# 'P'
	[ "'0QQ", "QQQQ" ],	# 'Q'
	[ "'&KR", "'&KR" ],	# 'R'
	[ "S\\S;", "S\\\\)" ],	# 'S'
	[ "BTTT", "BTTT" ],	# 'T'
	[ "LUUU", "LUUU" ],	# 'U'
	[ "LVVV", "LVVV" ],	# 'V'
	[ "LWQW", "LWWW" ],	# 'W'
	[ "LXXX", "LXXX" ],	# 'X'
	[ "LYYY", "LYYY" ],	# 'Y'
	[ "GZZZ", "GZZZ" ],	# 'Z'
	[ "[[[[", "[[[[" ],	# '['
	[ "\\\\\\\\", "\\\\\\\\" ],	# '\'
	[ "]]]]", "]]]]" ],	# ']'
	[ "AA  ", "^^^^" ],	# '^'
	[ "   O", "____" ],	# '_'
);

my @vdg = ();
my @t1 = ();

for my $j (0..$#font) {
	my ($chr_vdg,$chr_t1) = @{$font[$j]};
	my @bytes_vdg = ();
	my @bytes_t1 = ();
	push @bytes_vdg, 0x20;
	for my $i (0..3) {
		my $v_vdg = conv_vdg(ord(substr $chr_vdg, $i, 1));
		my $v_t1 = conv_t1(ord(substr $chr_t1, $i, 1));
		push @bytes_vdg, $v_vdg;
		push @bytes_t1, $v_t1;
	}
	push @bytes_vdg, 0x20;
	push @bytes_t1, 0x60, 0x60;
	push @vdg, \@bytes_vdg;
	push @t1, \@bytes_t1;
}

if ($variant eq 'vdg') {
	for (@vdg) {
		print "\t\tfcb ".join(",", map { sprintf "\$\%02x", $_ } @{$_})."\n";
	}
}

if ($variant eq 't1') {
	for (@t1) {
		print "\t\tfcb ".join(",", map { sprintf "\$\%02x", $_ } @{$_})."\n";
	}
}

exit 0;

# Stock VDG conversion

sub conv_vdg {
	my $v = shift;
	die if ($v < 0x20 || $v > 0x5f);
	return $v if ($v >= 0x20 && $v <= 0x3f);
	return ($v ^ 0x40) if ($v >= 0x40 && $v <= 0x5f);
	die "$v out of range for VDG conversions\n";
}

# T1 conversion - note $FF22 must have bits 4 and 5 set (lowercase, inverse
# video).  Because video is inverted, the translations are a little different.

sub conv_t1 {
	my $v = shift;
	return ($v ^ 0x40) if ($v >= 0x20 && $v <= 0x3f);
	return $v if ($v >= 0x40 && $v <= 0x5f);
	return 0x00 if ($v == 0x5e);  # '^'
	return 0x1f if ($v == 0x5f);  # '_'
	return ($v ^ 0x60) if ($v >= 0x61 && $v <= 0x7e);
	die "$v out of range for T1 conversions\n";
}

