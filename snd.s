; 2 square wave channels with variable duty cycle
; 12.3kHZ samplerate

		DATA

snd_note_table	fcb $01,$5d,$ab		; 0 = C3 - for no-clear lock
		; then semitone increments for combos:
		fcb $05,$76,$ab		;  1 = C5
		fcb $05,$c9,$d7		;  2 = C#5
		fcb $06,$21,$f4		;  3 = D5
		fcb $06,$7f,$4f		;  4 = D#5
		fcb $06,$e2,$37		;  5 = E5
		fcb $07,$4b,$00		;  6 = F5
		fcb $07,$ba,$04		;  7 = F#5
		fcb $08,$2f,$a3		;  8 = G5
		fcb $08,$ac,$40		;  9 = G#5
		fcb $09,$30,$46		; 10 = A5
		fcb $09,$bc,$25		; 11 = A#5
		fcb $0a,$50,$56		; 12 = B5
		fcb $0a,$ed,$57		; 13 = C6
		fcb $0b,$93,$ad		; 14 = C#6
		fcb $0c,$43,$e8		; 15 = D6
		fcb $0c,$fe,$9e		; 16 = D#6
		fcb $0d,$c4,$6d		; 17 = E6
		fcb $0e,$96,$00		; 18 = F6
		fcb $0f,$74,$09		; 19 = F#6
		fcb $10,$5f,$46		; 20 = G6
		fcb $11,$58,$80		; 21 = G#6
		fcb $12,$60,$8c		; 22 = A6
		fcb $13,$78,$4b		; 23 = A#6
		fcb $14,$a0,$ac		; 24 = B6
		fcb $15,$da,$ae		; 25 = C7

; sfx format is: duration-1 ($ff to end), note, then 'duration' pairs of
; form: amplitude,duty

sfx_bup		fcb 2,  0, $7c,$80, $3e,$90, $1e,$a0, $ff

sfx_bibibop	fcb 2,  1, $3e,$70, $7c,$80, $3e,$90
sfx_bibop	fcb 2,  5, $3e,$70, $7c,$80, $3e,$90
sfx_bop		fcb 3,  8, $3e,$70, $7c,$80, $3e,$90, $1e,$a0, $ff

sfx_bibibibop	fcb 1,  1, $3e,$70, $7c,$80
		fcb 2,  5, $3e,$70, $7c,$80, $3e,$90
		fcb 2,  8, $3e,$70, $7c,$80, $3e,$90
		fcb 3, 13, $3e,$70, $7c,$80, $7c,$90, $1e,$a0, $ff

		CODE

		align 256

; Entry:
; 	X = duration

snd_c1sfx_dur	fcb $ff
snd_c2sfx_dur	fcb $ff

snd_playfrag	lda #*>>8
		tfr a,dp
		setdp *>>8

; Got plenty of space to do fragment processing first

snd_c1sfx	equ *+1
		ldu #$0000
		lda snd_c1sfx_dur
		bmi 30F
		deca
		bmi 10F
		sta snd_c1sfx_dur
		bra 20F
10		ldd ,u++		; duration,note
		sta snd_c1sfx_dur
		bpl 15F
		clr snd_c1att
		bra 30F
15		ldx #snd_note_table
snd_c1noff	equ *+1
		addb #$00		; transpose
		lslb			; multiply note
		addb -1,u		; by three
		addb snd_c1noff		; this too
		abx
		lda ,x
		sta snd_c1freq0
		ldd 1,x
		std snd_c1freq1
20		pulu d			; amplitude,duty
		sta snd_c1att
		stb snd_c1duty
		stu snd_c1sfx
30

snd_c2sfx	equ *+1
		ldu #$0000
		lda snd_c2sfx_dur
		bmi 30F
		deca
		bmi 10F
		sta snd_c2sfx_dur
		bra 20F
10		ldd ,u++		; duration,note
		sta snd_c2sfx_dur
		bpl 15F
		clr snd_c2att
		bra 30F
15		ldx #snd_note_table
snd_c2noff	equ *+1
		addb #$00		; transpose
		lslb			; multiply note
		addb -1,u		; by three
		addb snd_c2noff		; this too
		abx
		lda ,x
		sta snd_c2freq0
		ldd 1,x
		std snd_c2freq1
20		pulu d			; amplitude,duty
		sta snd_c2att
		stb snd_c2duty
		stu snd_c2sfx
30

		ldu #reg_pia1_pdra
		leay -29,u		; y = reg_pia0_crb
		lda -1,y		; clear outstanding IRQ

snd_core_loop

snd_c1phas1	equ *+1
		ldd #$0000		; 3
snd_c1freq1	equ *+1
		addd #$0000		; +4 = 7
		std snd_c1phas1		; +5 = 12
snd_c1phas0	equ *+1
		ldb #$00		; +2 = 14
snd_c1freq0	equ *+1
		adcb #$00		; +2 = 16
		stb snd_c1phas0		; +4 = 20
snd_c1duty	equ *+1
		addb #$80		; +2 = 22
		rorb			; +2 = 24
		sex			; +2 = 26
snd_c1att	equ *+1
		anda #$00		; +2 = 28
		sta snd_c1		; +4 = 32

snd_c2phas1	equ *+1
		ldd #$0000		; 3
snd_c2freq1	equ *+1
		addd #$0000		; +4 = 7
		std snd_c2phas1		; +5 = 12
snd_c2phas0	equ *+1
		ldb #$00		; +2 = 14
snd_c2freq0	equ *+1
		adcb #$00		; +2 = 16
		stb snd_c2phas0		; +4 = 20
snd_c2duty	equ *+1
		addb #$80		; +2 = 22
		rorb			; +2 = 24
		sex			; +2 = 26
snd_c2att	equ *+1
		anda #$00		; +2 = 28

snd_c1		equ *+1
		adda #$00		; +2 = 30
		sta ,u			; +4 = 34

		; It might be better if this was fixed-duration (for
		; consistency), but we need a better idea of how much time
		; we _always_ have for that.  For now, just loop until
		; next vsync.

		lda ,y			; 4
		bpl snd_core_loop	; +3 = 7

					; total = 32+32+7 = 73 cycles
					; rate = 12.3kHz
		clra
		tfr a,dp
		rts

		setdp 0

; Schedule playback of a sound effect.

; Entry:
; 	A -> transpose in semitones (ie, combo)
; 	X -> sfx

snd_playsfx
		; prefer channel 2
		cmpa #12
		bls 5F
		lda #12		; max transpose
5		ldb snd_c2sfx_dur
		bpl 10F
		clr snd_c2sfx_dur
		sta snd_c2noff
		stx snd_c2sfx
		rts
10		ldb snd_c1sfx_dur
		bpl 99F
		clr snd_c1sfx_dur
		sta snd_c1noff
		stx snd_c1sfx
99		rts
