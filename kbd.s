; Adapted from my more full fledged key scanning routine.  No lowercase
; required here, and clear+key is much simplified.

		section "DP"

kbd_tmp0	rmb 1
kbd_tmp1	rmb 1
kbd_coco	rmb 1		; 0=dragon, 16=coco

		BSS

kbd_rollover	rmb 8

		CODE

kbd_waitkey
!		bsr kbd_scan
		beq <
		rts

kbd_init	ldx #kbd_rollover
		ldd #$0008
!		sta ,x+
		decb
		bne <
		; do an initial scan and populate the rollover table
		; fall through to kbd_scan

; long(ish) but decent keyboard scanning routine
kbd_scan	pshs b,x
		; check for any newly pressed keys
		clr kbd_tmp0
		ldx #kbd_rollover
		ldb #$fe
10		stb kbd_tmp1
		stb reg_pia0_pdrb
		lda reg_pia0_pdra
		ora #$80
		coma
		tfr a,b
		ora ,x
		eora ,x
		stb ,x+
		beq 30F
		ldb #$f8
20		addb #8
		lsra
		bcs 40F
		bne 20B
30		inc kbd_tmp0
		ldb kbd_tmp1
		lslb
		orb #$01
		bcs 10B
		bra 98F
		; have a scancode - find its position in the matrix
40		addb kbd_tmp0
		cmpb #$30
		bhs 50F
		addb kbd_coco
		cmpb #$30
		blo 50F
		subb #$30
50		; abort if any joystick firebutton is pressed
		lda #$ff
		sta reg_pia0_pdrb
		lda reg_pia0_pdra
		ora #$80
		coma
		bne 98F
		; find base matrix position
		ldx #kbd_matrix
		abx
		; check shift state (shift, clear)
		ldd #$7f40
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra	; shift pressed?
		bne 60F			; no? check clear
		leax 56,x		; yes? use shift table
		bra 80F
60		lda #$fd
		sta reg_pia0_pdrb
		bitb reg_pia0_pdra	; clear pressed?
		bne 80F			; no? normal keypress
		lda ,x
		cmpa #'/'		; clear+'/'?
		bne 70F
		lda #'\'		; yes? special case '\'
		puls b,x,pc
70		anda #$3f		; no? mask off bit 6
		puls b,x,pc
80		lda ,x			; fetch from table
		puls b,x,pc
98		clra			; report no keypress
99		puls b,x,pc

		DATA

; Unshifted and shifted keyboard matrix lookup table.

kbd_matrix
		; unshifted keys
		fcb	 '0,  '1,  '2,  '3,  '4,  '5,  '6,  '7
		fcb	 '8,  '9,  ':, $3b, $2c,  '-,  '.,  '/
		fcb	 '@,  'A,  'B,  'C,  'D,  'E,  'F,  'G
		fcb	 'H,  'I,  'J,  'K,  'L,  'M,  'N,  'O
		fcb	 'P,  'Q,  'R,  'S,  'T,  'U,  'V,  'W
		fcb	 'X,  'Y,  'Z,  '^, $0a, $08, $09, $20
		fcb	$0d,   0, $03,   0,   0,   0,   0,   0
		; shift + key
		fcb	  0,  '!,  '",  '#,  '$,  '%,  '&,  ''
		fcb	 '(,  '),  '*,  '+,  '<,  '=,  '>,  '?
		fcb	$13,  'A,  'B,  'C,  'D,  'E,  'F,  'G
		fcb	 'H,  'I,  'J,  'K,  'L,  'M,  'N,  'O
		fcb	 'P,  'Q,  'R,  'S,  'T,  'U,  'V,  'W
		fcb	 'X,  'Y,  'Z,  '_,  '[, $7f,  '], $20
		fcb	$0d, $0c, $03,   0,   0,   0,   0,   0
