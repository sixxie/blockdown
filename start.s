; Initialisation, machine detection, etc.

		section "DEFS"

sta_sam_f	macro
		sta reg_sam_f0c+(\2*2)+((\1>>(9+\2))&1)
		endm

KEYTBL_ENTRY	macro
		fcb ~(1<<\1)	; column
		fcb ~(1<<\2)	; row
		endm

stack_top	equ $0400

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; INIT code (and data) is ORGed below the main program, within what will
; end up being used as the frame buffer.  Nothing in this section will be
; accessible after initialisation is complete.

		section "INIT"

INIT_start

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; This is always applied first, but data within it may be overwritten if
; CoCo is detected.

dragon_init_data

		fcb 20F-10F
		fdb plr1_keys
		; Player 1 keys
10		KEYTBL_ENTRY 2,0	; '2' = hold
		KEYTBL_ENTRY 1,0	; '1' = r/cw
		KEYTBL_ENTRY 3,4	; 'S' = r/acw
		KEYTBL_ENTRY 3,0	; '3' = h/drop
		KEYTBL_ENTRY 0,5	; 'X' = s/drop
		KEYTBL_ENTRY 3,2	; 'C' = right
		KEYTBL_ENTRY 2,5	; 'Z' = left
		; Player 2 keys
		KEYTBL_ENTRY 0,1	; '8' = hold
		KEYTBL_ENTRY 7,0	; '7' = r/cw
		KEYTBL_ENTRY 3,3	; 'K' = r/acw
		KEYTBL_ENTRY 1,1	; '9' = h/drop
		KEYTBL_ENTRY 4,1	; '<' = s/drop
		KEYTBL_ENTRY 6,1	; '>' = right
		KEYTBL_ENTRY 5,3	; 'M' = left
20

		fcb 20F-10F
		fdb vdg_per_machine
10		fdb dragon_t_yx_to_y
		fdb dragon_t_cls
		fdb dragon_t_clr_line
		fdb dragon_t_putstr_yx
		fdb dragon_t_putstr
		fdb dragon_t_putchr_y
		fdb dragon_t_putbcd
		fdb dragon_g_cls
		fdb dragon_g_putdigit
		fdb dragon_g_putbcd
20

		fcb 20F-10F
		fdb vdg_nfonts
		; Two fonts available ('vdg' and 'askew'), with 'askew'
		; selected.  Overridden when a CoCo is detected.
10		fcb 2,1			; font selection
20

		fcb 0

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Applied for any detected CoCo.

coco_init_data

		fcb 20F-10F
		fdb plr1_keys
		; Player 1 keys
10		KEYTBL_ENTRY 2,4	; '2' = hold
		KEYTBL_ENTRY 1,4	; '1' = r/cw
		KEYTBL_ENTRY 3,2	; 'S' = r/acw
		KEYTBL_ENTRY 3,4	; '3' = h/drop
		KEYTBL_ENTRY 0,3	; 'X' = s/drop
		KEYTBL_ENTRY 3,0	; 'C' = right
		KEYTBL_ENTRY 2,3	; 'Z' = left
		; Player 2 keys
		KEYTBL_ENTRY 0,5	; '8' = hold
		KEYTBL_ENTRY 7,4	; '7' = r/cw
		KEYTBL_ENTRY 3,1	; 'K' = r/acw
		KEYTBL_ENTRY 1,5	; '9' = h/drop
		KEYTBL_ENTRY 4,5	; '<' = s/drop
		KEYTBL_ENTRY 6,5	; '>' = right
		KEYTBL_ENTRY 5,1	; 'M' = left
20

		fcb 20F-10F
		fdb vdg_nfonts
		; CoCo 1/2: Include extra 'askew_t1' font, but must
		; default to 'vdg', as we cannot detect the T1.
10		fcb 3,0			; font selection
20

		fcb 0

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Applied for CoCo 3 only.

coco3_init_data

		; CoCo 3: Override text routines.  Note these ignore all
		; font selections, as we can't use them on this machine.
		fcb 20F-10F
		fdb vdg_per_machine
10		fdb coco3_t_yx_to_y
		fdb coco3_t_cls
		fdb coco3_t_clr_line
		fdb coco3_t_putstr_yx
		fdb coco3_t_putstr
		fdb coco3_t_putchr_y
		fdb coco3_t_putbcd
		fdb coco3_g_cls
		fdb coco3_g_putdigit
		fdb coco3_g_putbcd
20

		fcb 0

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Gravity and delay data.  One set for 50Hz and one for 60Hz machines.

fps50_init_data
		fcb 20F-10F
		fdb gravity_curve
10

; Converted gravity curve to 50Hz.  Format is frames to wait, then lines
; to fall.
gravity_50	fcb 50,1
		fcb 40,1
		fcb 31,1
		fcb 24,1
		fcb 18,1
		fcb 13,1
		fcb 9,1
		fcb 7,1
		fcb 5,1
		fcb 3,1
		fcb 2,1
		fcb 1,1
		fcb 1,1
		fcb 1,2
		fcb 1,3
		fcb 1,20

; Various delays start to fall after level 15 to progress towards a "kill
; screen" affair.

delays_50
		; Levels 0-15
		fcb 25		; Entry delay (normal)
		fcb 25		; Entry delay (line clear)
		fcb 25		; Lock delay
		fcb 15		; Lock resets
		fcb 12		; Initial DAS
		fcb 2		; Repeat DAS
		;
		fcb 25,17,25,14,8,2	; level 16
		fcb 17,13,25,13,8,2	; level 17
		fcb 13, 7,25,12,8,1	; level 18
		fcb 13, 7,17,11,7,1	; level 19
		fcb 13, 7,17, 8,7,1	; level 20
		fcb  9, 3,13, 6,7,1	; level 21 (kill?)
		fcb  7, 3, 9, 4,7,1	; level 22 (KILL?)
		fcb  5, 3, 7, 2,7,1	; level 23 (KILL!)

vrate_text_50	fci " PAL",0
20

		fcb 0

fps60_init_data
		fcb 20F-10F
		fdb gravity_curve
10

; Standard gravity curve at 60Hz

gravity_60	fcb 60,1
		fcb 48,1
		fcb 37,1
		fcb 28,1
		fcb 21,1
		fcb 16,1
		fcb 11,1
		fcb 8,1
		fcb 6,1
		fcb 4,1
		fcb 3,1
		fcb 2,1
		fcb 1,1
		fcb 1,1
		fcb 1,2
		fcb 1,20

delays_60
		; Levels 0-15
		fcb 30		; Entry delay (normal)
		fcb 30		; Entry delay (line clear)
		fcb 30		; Lock delay
		fcb 15		; Lock resets
		fcb 14		; Initial DAS
		fcb 2		; Repeat DAS
		;
		fcb 30,20,30,14,9,2	; level 16
		fcb 20,16,30,13,9,2	; level 17
		fcb 16, 9,30,12,9,1	; level 18
		fcb 16, 9,20,11,8,1	; level 19
		fcb 16, 9,20, 8,8,1	; level 20
		fcb 11, 4,16, 6,8,1	; level 21 (kill?)
		fcb  8, 4,11, 4,8,1	; level 22 (KILL?)
		fcb  6, 4, 8, 2,8,1	; level 23 (KILL!)

vrate_text_60	fci "NTSC",0
20

		fcb 0

setup_stack	fcb $f0		; CC
		fcb $34		; A
		fcb $3c		; B
		fcb $ff		; DP
		fdb stack_top	; S
		;
		fcb $55		; A = restart flag
		fcb $00		; DP
		fdb start	; X = restart addr

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

copy_block
10		ldb ,u+
		bne 20F
		rts
20		pulu x
30		lda ,u+
		sta ,x+
		decb
		bne 30B
		bra 10B

		; This odd way of initialising actually ends up taking
		; exactly the same amount of space as the manual way, but
		; I've never done it before so another good disguise?

start		nop
		ldu #setup_stack
		pulu cc,a,b,dp,s
		setdp $ff

		sta reg_pia0_cra	; HS disabled
		sta reg_pia1_cra	; printer FIRQ disabled
		stb reg_pia1_crb	; CART FIRQ disabled
		inca
		sta reg_pia0_crb	; FS enabled hi->lo

		pulu a,dp,x
		setdp $00
		stx $0072
		sta $0071

		clr kbd_coco

		; Always do this - some may be overwritten by coco3
		; equivalent later.
		ldu #dragon_init_data
		bsr copy_block

		; Detect CoCo
		lda $a000		; [$A000] points to ROM1 in CoCos
		anda #$20
		beq 10F
		lda #$10
		sta kbd_coco
		; U -> coco_init_data
		bsr copy_block

		lda $80fd		; '1' for CoCo1/2, '2' for CoCo 3
		cmpa #'2
		bne 10F
		; U -> coco3_init_data
		bsr copy_block

10

		; Initialise random pool
		clr rnd_state
		clr rnd_next
		ldd #$6309
		std rnd_lfsr32
		ldd #$6809
		std rnd_lfsr32+2
		ldd #$6883
		std rnd_lfsr31
		ldd $0112		; from TIMER
		std rnd_lfsr31+2
		; fill pool
10		jsr rnd_generate
		lda rnd_next
		beq 10B
10		jsr rnd_generate
		lda rnd_next
		bne 10B

		; NTSC detection
		ldu #fps50_init_data
		ldx #0
		lda reg_pia0_pdrb	; clear outstanding IRQ
		sync			; wait for FS hi->lo
		lda reg_pia0_pdrb	; clear outstanding IRQ
10		lda reg_pia0_crb	; 5 - test for IRQ
		bmi 20F			; 3
		mul			; 11 - waste time...
		mul			; 11
		mul			; 11
		leax ,x			; 4
		leax ,x			; 4
		leax 1,x		; 5 - inc line count
		bra 10B			; 3 = 57 (cycles per scanline)
20		cmpx #288		; PAL = 312, NTSC = 262
		bhs 30F
		ldu #fps60_init_data
30		jsr copy_block

		; High score table initialisation
		ldx #highscores_normal
		lda #num_lbe*2
		sta tmp0
10		ldu #name_init
		ldb #sizeof_lbe
!		lda ,u+
		sta ,x+
		decb
		bne <
		dec tmp0
		bne 10B

		; Game options initialisation
		clr nplayers		; 1-player
		clr garbage_style	; easy
		lda #10
		sta game_speed

		; All done - subsequent resets don't need to do any of
		; this preinit stuff.
		ldx #restart
		stx $0072
		jmp ,x

name_init	fci "BLOCKDOWN    ", $00,$00,$10,$00, $00

INIT_end
INIT_size	equ INIT_end-INIT_start

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Now follows the code jumped to on reset.  This has to hang around.

		DATA

restart_stack	fcb $f0		; CC
		fcb $34		; A
		fcb $3c		; B
		fcb $ff		; DP
		fdb stack_top	; S
		;
		fcb $00		; DP

		CODE

restart		nop
		ldu #restart_stack
		pulu cc,a,b,dp,s
		setdp $ff

		sta reg_pia0_cra	; HS disabled
		sta reg_pia1_cra	; printer FIRQ disabled
		stb reg_pia1_crb	; CART FIRQ disabled
		inca
		sta reg_pia0_crb	; FS enabled hi->lo

		; SAM MPU rate = slow
		sta reg_sam_r1c
		sta reg_sam_r0c

		; VDG mode 0, CSS1
		lda #$08
		sta reg_pia1_pdrb

		; SAM mode 4 (3K)
		sta reg_sam_v0c
		sta reg_sam_v1c
		sta reg_sam_v2s

		sta_sam_f fb0,0
		sta_sam_f fb0,1
		sta_sam_f fb0,2
		sta_sam_f fb0,3
		sta_sam_f fb0,4
		sta_sam_f fb0,5
		sta_sam_f fb0,6

		pulu dp
		setdp $00

		ldx #$0400
		lda #$20
!		sta ,x+
		cmpx #$0600
		blo <
		ldx #t_paused
		ldy #$04ac
		jsr coco3_t_putstr_y
		ldy #$04ec
		jsr coco3_t_putstr_y
		ldy #$052c
		jsr coco3_t_putstr_y

		jsr kbd_init

		jmp main

		DATA

t_paused	fci "*PAUSED*",0
                fci "SPACE TO",0
                fci "CONTINUE",0
