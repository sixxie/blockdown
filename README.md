# Blockdown

Falling blocks puzzle game for the Dragon 32/64 and Tandy Colour Computer. 1 or
2 players! Written in 100% Machine Code!

by Ciaran Anscomb, 2020-2021.

Redistribution encouraged! Just be sure to include the documentation with any
packaging.


## Building

Only tested under Linux.  Building requires:

 * asm6809
 * dzip
 * Perl
 * C compiler
 * libSDL and libSDL\_image

Packaging requires:

 * bin2cas.pl
 * Toolshed
