TIMING_DEBUG	set 0
GARBAGE_DEBUG	set 0

; BUGS
;
; Surely there are some?

; Important (add or fix before release)

; None?

; Nice to have (not required for first 16K compatible release)

; TODO: detect & reward "perfect clear"
;  - annoying to add as it stands
; TODO: starting level option
; TODO: starting garbage lines count
; TODO: time attack game mode
; TODO: puzzle games
;  - predictable sequence of pieces with set garbage
;  - usually needs perfect clear, but number of lines maybe equivalent?
; TODO: rethink 2-player levelling
;  - maybe share a line count and level?
;  - or make levelling up time-based.  could have pips alert that a level
;    bump is coming!

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		include "dragonhw.s"

		section "DEFS"

fb0		equ $0e00
sizeof_fb0	equ 3072
fb0_end		equ fb0+sizeof_fb0

stack_nlines	equ 40		; 20 visible, 2 for stack base
stack_width	equ 13		; 10 + 3 guard cells for edges

		; order sections
		section "DP"
DP_start	org $0074
		section "INIT"
		org fb0_end-INIT_size
		CODE
		setdp $00
CODE_start	org INIT_end
DATA_start	DATA
BSS_start	BSS

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; include snd.s first as it contains self-modifying code
		; that depends on setting DP
		include "snd.s"
		include "start.s"
		include "title.s"
		include "kbd.s"
		include "vdg.s"
		include "pieces.s"
		include "players.s"
		include "rnd.s"
		include "dunzip8.s"

		section "DP"

tmp0		rmb 2
tmp1		rmb 2
tmp2		rmb 2
tmp3		rmb 2

nplayers	rmb 1		; 0 = 1-player, else 2-player
game_speed	rmb 1		; 5 or 10 (lines per level)
garbage_style	rmb 1		; 0 = easy, else hard

		DATA

jtbl_game_state	fdb game_state_startup
		fdb game_state_normal
		fdb game_state_lock
		fdb game_state_clear
		fdb game_state_fall
		fdb game_state_entry
		fdb game_state_gameover
		fdb game_state_hold

		CODE

; 'start' will do all the machine initialisation then jump here

start_game

		jsr [vdg_g_cls]
		jsr init_players

gameloop

		jsr rnd_generate

		; wait for FS to indicate end of active area.  try and do
		; everything involving the piece itself in the vsync time.

	if TIMING_DEBUG
		; DEBUG: a "blip" to show where we finished
		ldd #$f808
		sta reg_pia1_pdrb
		stb reg_pia1_pdrb
	endif

		; As yet undecided about whether a fixed-duration fragment
		; player would be better, but for now, this plays until
		; the next vsync:
		jsr snd_playfrag

	if TIMING_DEBUG
		; DEBUG: different video mode for vsync
		lda #$f8
		sta reg_pia1_pdrb
	endif

		; Undraw everything first, giving a slight delay from the
		; bottom of the frame to when the keys are polled allowing
		; a tiny bit more reaction time.

		ldu #plr1
		jsr undraw_piece	; also undraws ghost
		lda nplayers
		beq >
		ldu #plr2
		jsr undraw_piece	; also undraws ghost
!

		; Now scan the controls.  They may not be needed depending
		; on player state, but it's important to poll them at the
		; exact same time each frame.
		jsr scan_keyboard

		; Now process each player's game state
		ldu #plr1
		ldx #jtbl_game_state
		ldb plr_state,u
		jsr [b,x]
		lda nplayers
		beq >
		ldu #plr2
		ldx #jtbl_game_state
		ldb plr_state,u
		jsr [b,x]
!

	if GARBAGE_DEBUG
		; DEBUG: scan for '4'
		ldx #reg_pia0_pdra
		ldd #$effe
		sta 2,x
		orb ,x			; OR everything but test bit
		subb #$ff		; SUB sets CC.C if key down
		bcc 10F
		lda #1
		sta plr2+plr_grb_send
		; DEBUG: scan for '5'
10		ldd #$dffe
		sta 2,x
		orb ,x			; OR everything but test bit
		subb #$ff		; SUB sets CC.C if key down
		bcc 90F
		lda #1
		sta plr1+plr_grb_send
90
	endif

		; now process the things we don't really care about
		; happening during vsync

	if TIMING_DEBUG
		; DEBUG: reset video mode
		lda vdg_mode
		sta reg_pia1_pdrb
	endif

		ldu #plr1
		jsr update_player
		lda nplayers
		beq >
		ldu #plr2
		jsr update_player
		; send garbage only in 2-player mode!
		jsr send_garbage
		ldu #plr1
		jsr send_garbage
!

		; CLEAR + key = special function.  For now, only use
		; CLEAR+BREAK to end game, but it might be useful to
		; enable/disable SFX/music.

		; test for clear
		ldx #reg_pia0_pdra
		ldd #$fd40
		sta 2,x
		bitb ,x
		bne gameloop

		; clear+enter = pause
test_pause	lda #$fe
		sta 2,x
		bitb ,x
		beq do_pause

		; clear+break = quit
test_quit	lda #$fb
		sta 2,x
		bitb ,x
		beq do_quit

		bra gameloop

do_quit		lda #state_gameover
		cmpa plr1+plr_state
		beq >
		cmpa plr2+plr_state
		beq >
		ldu #plr1
		jsr undraw_set_plr_game_over
		lda nplayers
		beq >
		ldu #plr2
		jsr undraw_set_plr_game_over
		clr plr_winner,u		; no winner here...
		clr plr1+plr_winner
!		jmp gameloop

do_pause
		; The regular text screen at $0400 contains the paused
		; message - just switch to it!
		sta reg_sam_v2c
		sta_sam_f $0400,0
		sta_sam_f $0400,2
		ldd #$080f
		sta reg_pia1_pdrb
		stb $ff9c
		; Wait for spacebar...
!		jsr kbd_scan
		cmpa #32
		bne <
		; Set video back to game screen in right mode.
		sta_sam_f $0e00,0
		sta_sam_f $0e00,2
		sta reg_sam_v2s
		lda vdg_mode
		sta reg_pia1_pdrb
		lda #9
		sta $ff9c

!		jmp gameloop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DP"

need_ghost	rmb 1
was_locked	rmb 1

		CODE

; Test keyboard controls for player (previously scanned by scan_keyboard).

; Entry:
; 	U -> player

test_controls

		; Testing for rotation before movement should allow for
		; "synchro" moves (assuming I ever let G exceed 1).
		; Tested at low framerate and this works as expected.

		clra
		sta need_ghost
		ldx plr_pc_stck,u
		cmpx plr_gh_stck,u
		beq 10F
		inca
10		sta was_locked

		; test for r/acw
		lda plr_kbd_new,u
20		bita #kbit_racw
		beq 30F
		jsr rotate_acw
		beq 88F
		bra 90F

		; test for r/cw
30		bita #kbit_rcw
		beq 90F
		jsr rotate_cw
		beq 88F
		bra 90F

		; A successful rotation may have changed more than the
		; orientation; wall kicks could have offset all position
		; data, so copy everything.
88		ldd pct_odata
		std plr_pc_odata,u
		ldd pct_stck
		std plr_pc_stck,u
		ldd pct_scrn
		std plr_pc_scrn,u
		inc need_ghost
		ldx plr_pcq,u
		cmpx #pc_t		; is there a T?
		bne 90F			; no? shark attack (no t-spin)
		inc plr_tspin,u		; rotate: possible t-spin bonus
90

		; test for left, implement autorepeat
		lda plr_kbd_state,u
		anda plr_kbd_mmask,u
		bita #kbit_left
		beq 20F
		dec plr_kbd_ldas,u
		bmi 10F
		bne 30F
		ldb plr_lvl_rdas,u	; REPEAT DAS
		bra 11F
10		ldb plr_lvl_idas,u	; INIT DAS
11		stb plr_kbd_ldas,u
		lda #$20
		bra 40F
20		clr plr_kbd_ldas,u
30

		; test for right, implement autorepeat
		bita #kbit_right
		beq 20F
		dec plr_kbd_rdas,u
		bmi 10F
		bne 30F
		ldb plr_lvl_rdas,u	; REPEAT DAS
		bra 11F
10		ldb plr_lvl_idas,u	; INIT DAS
11		stb plr_kbd_rdas,u
		lda #$10

		; Moving left or right will only ever affect the
		; x-position and screen address.  No need to copy
		; unchanged orientation data.
40		sta pct_xyoff
		jsr test_piece
		bne 30F
		ldd pct_scrn
		std plr_pc_scrn,u
		ldd pct_stck
		std plr_pc_stck,u
		inc need_ghost
		clr plr_tspin,u		; move: no t-spin bonus
		bra 30F

20		clr plr_kbd_rdas,u
30

		lda need_ghost
		beq 30F
		jsr find_ghost
		; x = ghost stack position
		cmpx plr_pc_stck,u
		bne 10F
		; now locked: reset lock delay if allowed
		lda plr_lockreset,u	; any lock resets left?
		beq 30F			; no? we're done here
		dec plr_lockreset,u
5		lda plr_lvl_lk_dly,u	; LOCK DELAY
		bra 20F
		; now not locked: if it was before, reset gravity delay
10		lda was_locked
		bne 30F
		lda plr_lvl_gdelay,u
20		sta plr_delay,u
30

		; test for soft drop
		lda plr_kbd_state,u
		bita #kbit_sdrop
		beq 20F
		dec plr_kbd_srpt,u
		bmi 10F
		bne 30F
10		ldb #2
		stb plr_kbd_srpt,u

		; soft drop
		lda #$40
		sta pct_xyoff
		jsr test_piece
		bne 30F
		lda #$01
		adda plr_scoreacc,u
		daa
		sta plr_scoreacc,u
		ldd pct_scrn
		std plr_pc_scrn,u
		ldd pct_stck
		std plr_pc_stck,u
		clr plr_tspin,u		; drop: no t-spin bonus

		; if we now == ghost pos, reset the lock delay
		cmpd plr_gh_stck,u
		bne 30F
		; reset lock delay
		lda plr_lvl_lk_dly,u	; LOCK DELAY
		sta plr_delay,u
		bra 30F

20		clr plr_kbd_srpt,u
30

		; test for hard drop
90		lda plr_kbd_new,u
		bita #kbit_hdrop
		beq 50F
		; first, if gh == pc, then this does does not score, but
		; also does not cancel t-spin possibility.
		ldx plr_gh_stck,u
		cmpx plr_pc_stck,u
		bls 10F
		; add 2 * drop distance to score.  no easy way to divide
		; the difference in stack addresses by _13_, sadly, so
		; iterate:
		lda plr_scoreacc,u
!		adda #$02
		daa
		leax -stack_width,x
		cmpx plr_pc_stck,u
		bhi <
		clr plr_tspin,u
		sta plr_scoreacc,u
10

		; Ghost piece data will be used to lock piece, so move
		; straight to state_lock.
		lda #state_lock
		sta plr_state,u
99		rts

		; test for hold
50		bita #kbit_hold
		beq 99B
		ldb plr_has_held,u	; already swapped this turn?
		bne 99B			; yes? disallow
		sta plr_has_held,u
		sta plr_f_drawheld,u	; flag draw of held piece
		clr plr_scoreacc,u	; clear any accumulated score
		ldd plr_pcq,u		; swap held
		ldx plr_pc_held,u	; ... and top
		std plr_pc_held,u	; ... pieces
		stx plr_pcq,u		; ... this tests if old held = 0
		; Move to a special variant of the entry state that does
		; something slightly different if the swap was not with a
		; valid piece.
		lda #state_hold
88		sta plr_state,u
		lda plr_lvl_entr,u	; ENTRY DELAY
		sta plr_delay,u
		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; First game state.  Does a few things before handing control to
; state_entry.

		CODE

game_state_startup
		inc plr_delay,u
		lda plr_delay,u
		deca
		; Draw initial level number
		lbeq update_level
10		cmpa #6
		bhi 20F
		; Fetch new piece into queue (6 times)
		inc plr_f_drawnext,u
		jmp fetch_piece
		; Finally, move to state_entry
20		lda #state_entry
		bra 88B

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Normal in-game state.  Applies player controls and gravity.  Tests for
; piece lock and, after lock delay, moves to state_lock.

		CODE

game_state_normal

		jsr test_controls

		; Garbage receiving.
		lda plr_f_scrn,u	; already scrolling screen?
		bne 10F			; yes? skip receiving for now
		lda plr_grb_recv,u	; any to receive?
		beq 10F			; no? skip this then
		dec plr_grb_recv,u	; 1 line at a time...

		; For now all the piece manipulation is outside of
		; raise_stack as I might put receiving into other states
		; too.  There's something I'm not checking for here:
		; whether the active piece is pushed above the top of the
		; stack.  As there are 20 lines of hidden stack, I'm
		; pretty sure that's not possible.
		jsr raise_stack
		ldd plr_gh_stck,u
		subd #stack_width
		std plr_gh_stck,u
		ldd plr_gh_scrn,u
		subd #32*4
		std plr_gh_scrn,u
		ldx plr_pc_stck,u
		cmpx plr_gh_stck,u
		bls 10F
		leax -stack_width,x
		stx plr_pc_stck,u
		std plr_pc_scrn,u
10

		ldx plr_pc_stck,u
		cmpx plr_gh_stck,u
		bne 20F

		; piece == ghost: handle lock delay

		dec plr_delay,u
		bne 10F
		ldb #state_lock
		stb plr_state,u
		jmp draw_piece

10		lda plr_delay,u
		lsra
		lsra
		lbcc draw_lock0
		jmp draw_lock1

		; piece != ghost: handle gravity

20		dec plr_delay,u
		bne 50F

		ldx plr_pc_stck,u
		ldy plr_pc_scrn,u
		ldb plr_lvl_glines,u
		lda #stack_width
		mul
		leax d,x
		ldb plr_lvl_glines,u
		lda #32*4
		mul
		leay d,y
		cmpx plr_gh_stck,u
		bls 25F
		ldx plr_gh_stck,u
		ldy plr_gh_scrn,u
25		sty plr_pc_scrn,u
		stx plr_pc_stck,u
		clr plr_tspin,u

		; if we now == ghost pos, reset the lock delay
		cmpx plr_gh_stck,u
		bne 30F
		; reset lock delay
		lda plr_lvl_lk_dly,u	; LOCK DELAY
		bra 40F
30		lda plr_lvl_gdelay,u	; GRAVITY DELAY
40		sta plr_delay,u

50		jsr draw_ghost
		jmp draw_piece

		fcc "ALEXEY"

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Lock state.  Lock delay has counted down to zero by this point.  Sets
; appropriate bits in the stack bitmap and tests for cleared lines.  Next
; state will either state_clear (animate clearing lines) or state_entry
; (new piece entry with delay).
;
; As the ghost piece _always_ represents where a piece will lock, we use
; its data - this means we can transfer straight to this state when user
; hard drops without having to duplicate any logic.

		CODE

game_state_lock

		; add score accumulated for dropping (soft or hard)
		lda plr_scoreacc,u
		adda plr_score+3,u
		daa
		sta plr_score+3,u
		lda plr_score+2,u
		adca #$00
		daa
		sta plr_score+2,u
		lda plr_score+1,u
		adca #$00
		daa
		sta plr_score+1,u
		lda plr_score,u
		adca #$00
		daa
		sta plr_score,u
		clr plr_scoreacc,u

		clr plr_has_held,u

		ldd plr_gh_scrn,u
		std plr_pc_scrn,u
		ldy plr_gh_stck,u
		ldx plr_pc_odata,u
		leax pco_stack_data,x
		lda plr_pc_colr,u
		ldb ,x
		sta b,y
		ldb 1,x
		sta b,y
		ldb 2,x
		sta b,y
		ldb 3,x
		sta b,y

		; Score index - incremented per-line.  If a T-spin is
		; detected, the base is shifted.
		clr tmp1		; track number of lines cleared
		clr tmp1+1		; use normal scoring

		; Detect T-spin.  Rules are 3-corner T:
		; - Must be a T piece
		; - Last movement was a rotate
		; - 3/4 corners (empty in the piece) occupied
		lda plr_tspin,u		; allowed to be a t-spin?
		beq 20F			; no? skip the checks then
		clra
		ldb ,y			; top-left
		lsrb
		adca #0
		ldb 2,y			; top-right
		lsrb
		adca #0
		ldb stack_width*2,y	; bottom-left
		lsrb
		adca #0
		ldb stack_width*2+2,y	; bottom-right
		lsrb
		adca #0
		cmpa #3			; at least 3 corners filled?
		blo 10F			; no? not a t-spin
		lda #4
		sta tmp1+1		; use t-spin scores
		bra 20F
		; If this is a valid T-spin, we'll need to know that when
		; sending garbage later.  If not, clear the flag now
		; (it'll be cleared by the next piece entry otherwise).
10		clr plr_tspin,u		; un-flag t-spin

20		jsr clip_piece	; draw only blocks within visible area

		jsr hide_player_piece

		; Detect line clears.  Flags the cleared lines in the
		; right wall of the stack ($00 = cleared).  These must
		; obviously be restored to wall status ($0f) later!

		ldx plr_gh_stck,u
		leax 2,x	; piece origin may be up to 2 cells into wall
!		lda ,-x
		cmpa #$0f
		bne <
		; ok, found left wall.  now test lines
		ldb #4			; scan at most four lines
		stb tmp0
10		lda ,x+			; convenient $0f from left wall
		cmpa #$ff		; is this actually base?
		beq 20F			; yes? done
		ldb #10
!		anda ,x+		; mask against 10 cells
		decb			; will become $00 if any
		bne <			; cell is empty
		eora #$0f		; now $0f=skip, $00=cleared
		sta ,x++		; record state in right wall!
		bne 15F
		inc tmp1		; inc line cleared count
15		dec tmp0
		bne 10B
20

		lda tmp1		; will be non-zero if any lines cleared
		beq 50F


20		lda #state_clear
		sta plr_state,u
		lda #8		; set to 16 to animate twice?
		sta plr_delay,u

		; Scoring.  A lookup from the "scores" table based on
		; number of lines cleared.  T-spins will have set the base
		; such that bigger scores are awarded.
		;
		; Top bit set in a score indicates it's "difficult".  Back
		; to back difficult clears are tracked and rewarded.

		; Combo calculation
		lda plr_combo,u
		sta tmp0		; record combo sfx transpose
		inc plr_combo,u	; inc after ld so that 1 clear = 0 bonus
		ldb plr_level,u
		incb
		mul
		std tmp2

		lda tmp1
		deca
		adda tmp1+1	; add scoring base (normal/t-spin)
5		ldb #sizeof_scoreent
		mul
		ldx #scores100
		abx
		lda ,x+			; A = no. hundreds
		lsla			; Now A = no. 50s
		rol plr_difficult,u
		ldb plr_difficult,u
		comb
		andb #$03
		bne 10F
		lsla
		adda -1,x
		adda -1,x	; cancels any $80 add!
		lsra
10		ldb plr_level,u
		incb
		mul
		addd tmp2	; add in combo calculation
		std plr_score50,u
		clr plr_score50_idx,u
		ldy plr_scrn,u
		leay 192+3,y
		jsr dragon_g_putstr_y
		ldx ,x
		lda tmp0	; combo sfx transpose
		jmp snd_playsfx

		; no lines cleared, but it may yet be a t-spin mini
50		lda #state_entry
		sta plr_state,u
		lda plr_lvl_entr,u	; ENTRY DELAY
		sta plr_delay,u
		clr plr_combo,u		; break any combo
		lda tmp1+1
		bne 60F
		ldx #sfx_bup
		clra
		jsr snd_playsfx
		rts

		; t-spin mini (no lines cleared)
60		ldd #$0000		; zero out combo calculation
		std tmp2
		sta tmp0		; combo sfx transpose
		lda #7			; use t-mini scoring
		bra 5B

		fcc "DMITRY"

		DATA

; Top bit of hundreds indicates "difficult".  In the code that uses this
; table, the number of hundreds is shifted left, both to test the
; "difficult" bit 7, and to multiply it by two to get a number of 50s.
sizeof_scoreent	equ 12

scores100	fcb 1			; no. hundreds
		fci " SINGLE ",0	; notify string
		fdb sfx_bop		; sfx ptr
		fcb 3
		fci " DOUBLE ",0
		fdb sfx_bibop
		fcb 5
		fci " TRIPLE ",0
		fdb sfx_bibibop
		fcb $80|8
		fci " TETRIS ",0
		fdb sfx_bibibibop
		;
		fcb $80|2
		fci "T-SINGLE",0
		fdb sfx_bop
		fcb $80|12
		fci "T-DOUBLE",0
		fdb sfx_bibop
		fcb $80|16
		fci "T-TRIPLE",0
		fdb sfx_bibibop
		;
		fcb 1
		fci " T-MINI ",0
		fdb sfx_bop

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; Line clear animation.  Fills cleared lines with white across four
; frames: pause, inner two, pause, outer two, then clears across four more
; frames in same order.  Lastly, moves to state_fall.

game_state_clear

		lda plr_delay,u
		deca
		sta tmp0+1
		bne 10F
		lda #state_fall
		sta plr_state,u

		lda #4
10		sta plr_delay,u

		ldx plr_stck,u
		leay 39*stack_width+13,x
		ldx plr_scrn,u
		leax 17*32+19*128+12,x

		lsr tmp0+1
		bcs 99F

		lsr tmp0+1
		bcc 10F
		ldd #$e0ea
		bra 20F
10		ldd #$00aa
20		std tmp1
		lsr tmp0+1
		bcc 30F
		ldd #$cfcf
		bra 40F
30		ldd #$8080
40		std tmp2

		lda #20
		sta tmp0

50		lda ,y
		bne 60F
		stx tmp3

		ldb tmp1
		leax b,x
		ldd tmp2
		std ,--x
		std ,--x
		std ,--x
		std ,--x
		std ,--x

		ldb tmp1+1
		leax b,x
		ldd tmp2
		std ,--x
		std ,--x
		std ,--x
		std ,--x
		std ,--x

		ldx tmp3

60		leax -4*32,x
		leay -stack_width,y
		dec tmp0
		bne 50B
99		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; After line clear animation, this makes the remaining lines fall into
; place.  Works over four frames: if only falling one line, it looks nice
; and smooth.  More lines looks a little more odd, but I'm happy enough
; with it.

game_state_fall
		ldx plr_stck,u
		leay 39*stack_width+13,x

		lda plr_delay,u
		deca
		bmi collapse_stack

		ldx plr_scrn,u
		leax 17*32+19*128+12,x
		ldb #-32
		mul
		leax b,x

		lda #20
		sta tmp0	; dest lines visible

10		lda ,y
		bne 20F
		leay -stack_width,y
		bra 10B

		fcc "VADIM"

20		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x

		leax -118,x
		leay -3,y

		dec tmp0
		bne 10B
		bra 80F

; When the animation is complete, this last step adjusts the stack bitmaps
; to remove the blank lines.  This is also the point at which we send the
; opponent garbage - yay!
;
; TODO: On-screen line count is also updated here, which means we've
; counted the number to add twice - rethink this...

collapse_stack	leax ,y
		lda #stack_nlines
		sta tmp0
		sta tmp0+1
		clr tmp1
10		dec tmp0	; more dest lines to fill?
		bmi 70F		; no, done
20		dec tmp0+1	; more source lines available?
		bmi 30F		; no, fill rest with blank
		lda ,y
		bne 25F
		dec plr_lvl_lines,u
		inc tmp1
		lda #$0f
		sta ,y		; reset wall
		leay -stack_width,y
		bra 20B
25		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		ldd ,--y
		std ,--x
		leay -3,y
		leax -3,x
		bra 10B
		; fill remaining lines with blank
30		ldd #$8080
40		std ,--x
		std ,--x
		std ,--x
		std ,--x
		std ,--x
		leax -3,x
		dec tmp0
		bpl 40B
70		lda plr_nlines+1,u
		adda tmp1
		daa
		sta plr_nlines+1,u
		lda plr_nlines,u
		adca #$00
		daa
		anda #$0f
		sta plr_nlines,u
		; amount of garbage to send
		lda tmp1
		deca
		ldb plr_tspin,u
		beq >
		adda #4
!		ldx #t_garbage
		lda a,x
		ldb plr_difficult,u
		comb
		andb #$03
		bne >
		inca		; +1 for B2B difficult clears
!		sta plr_grb_send,u

		; note: the main part of game_state_fall (above) exits
		; through here too.
80		dec plr_delay,u
		bpl 99F
		lda #state_entry
		sta plr_state,u
		lda plr_lvl_entr_lc,u	; ENTRY DELAY (LC)
		sta plr_delay,u
99		rts

		DATA

t_garbage	fcb 0,1,2,4		; 1-4 line clears
		fcb 2,4,6		; 1-3 t-spin line clears

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		DATA

; Blank line to overprint any line clear indicator.

blank		fcb $80,$80,$80,$80,$80,$80,$80,$80,0

		CODE

game_state_hold
		dec plr_delay,u
		beq 10F
		rts
10		ldx plr_pcq,u		; ... this tests if old held = 0
		beq 15F			; fetch new piece if so
		jsr next_piece_x	; else just take top one
		bra 20F

game_state_entry
		dec plr_delay,u
		beq 10F
		rts

10		ldy plr_scrn,u
		leay 192+3,y
		ldx #blank
		jsr dragon_g_putstr_y

		; fetch next piece
15		jsr next_piece

20		lda #state_normal
		sta plr_state,u
		lda plr_lvl_gdelay,u
		sta plr_delay,u

		; preset DAS if held
		lda plr_kbd_state,u
		anda plr_kbd_mmask,u
		bita #kbit_left
		beq 30F
		ldb #1		; ENTRY DAS PRESET
		stb plr_kbd_ldas,u
		bra 40F
30		bita #kbit_right
		beq 40F
		ldb #1		; ENTRY DAS PRESET
		stb plr_kbd_rdas,u
40

		clr plr_tspin,u

		; test for initial rotation
		lda plr_kbd_state,u
		bita #kbit_rcw		; rotate cw held?
		beq 30F			; no? test rotate acw
		jsr rot_enter_cw	; successful rotate cw?
		beq 35F			; yes? copy rotated data

		; try and use unrotated piece
10		clr pct_xyoff
		jsr test_piece		; piece ok where it is?
		beq 80F			; yes? done
		lda #$80		; no? try 1 row up
		sta pct_xyoff
		jsr test_piece_o	; piece ok here?
		beq 40F			; yes? copy position data
20		lda #$c0		; no? try 2 rows up
		sta pct_xyoff
		jsr test_piece_o	; piece ok here?
		bne set_plr_game_over	; no? game over, man
		bra 40F			; yes? copy position data

30		bita #kbit_racw		; rotate acw held?
		beq 10B			; no? test unrotated piece
		jsr rot_enter_acw	; successful rotate acw?
		bne 10B			; no? try unrotated piece

		; successful rotation
35		ldx plr_pcq,u
		cmpx #pc_t		; is there a T?
		bne 40F			; no? shark attack (no t-spin)
		inc plr_tspin,u		; rotate: possible t-spin bonus

		; copy rotated data into place
40		ldd pct_odata
		std plr_pc_odata,u
		ldd pct_stck
		std plr_pc_stck,u
		ldd pct_scrn
		std plr_pc_scrn,u
		jsr find_ghost

		; if we now == ghost pos, reset the lock delay
80		ldx plr_gh_stck,u
		cmpx plr_pc_stck,u
		bne 99F
		; reset lock delay
		lda plr_lvl_lk_dly,u	; LOCK DELAY
		sta plr_delay,u
99		rts

		; game over branched to above, but used elsewhere too

undraw_set_plr_game_over
		jsr undraw_piece	; also undraws ghost
		; fall through to set_plr_game_over

set_plr_game_over
		lda #state_gameover
		sta plr_state,u
		clr plr_delay,u
		ldb nplayers
		beq >
		ldx plr_other,u
		inc plr_winner,x
!
		; fall through to hide_player_piece

hide_player_piece
		lda #$80
		sta plr_pc_colr,u
		ldd #fb0+92*32+31	; bottom-right of screen
		std plr_pc_scrn,u
		std plr_gh_scrn,u
		ldd #dummy_draw_data
		std plr_pc_odata,u
		rts

		DATA

; Dummy data that instructs draw_* to draw only a single block (but 4
; times).  Screen address will also be updated to point to an usused area
; of the screen, and piece colour set to black.
;
; Optimisation note: can instead point this at any existing run of 5 zero
; bytes; there's bound to be one somewhere.

dummy_draw_data	fdb 0
		fcb 0,0,0

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Animate "game over".  Progressively fills every visible block from the
; bottom of the screen with white while displaying "GAME OVER" over the
; top.  Then waits for player 2 (if playing) to also be done before
; jumping to the title screen.  TODO: this should really be going to the
; high score table.

		CODE

game_state_gameover
		inc plr_delay,u
		lda plr_delay,u
		deca
		bne 50F
		; frame 0 of game over: turn visible blocks white
		ldx plr_stck,u
		leax 20*stack_width,x
		ldb #130
		stb tmp0
10		ldd ,x
		anda #$0f
		andb #$0f
		addd #$c0c0
		std ,x++
		dec tmp0
		bne 10B
		stx plr_go_stck,u
		ldx plr_scrn,u
		leax 19*128+17*32+12,x
		stx plr_go_scrn,u
99		rts

		; frames 1-80: draw one line of stack to screen
50		cmpa #80
		bhi 100F
		ldx plr_go_stck,u
		ldy plr_go_scrn,u
		ldd ,--x
		std ,--y
		ldd ,--x
		std ,--y
		ldd ,--x
		std ,--y
		ldd ,--x
		std ,--y
		ldd ,--x
		std ,--y
		leay -22,y
		sty plr_go_scrn,u
		lda plr_delay,u
		deca
		anda #3
		bne 10F
		leax -3,x
		stx plr_go_stck,u
10		ldx plr_scrn,u
		leay 8*192+5,x
		ldx #t_gameover
		jsr dragon_g_putstr_y
		leay 188,y
		jmp dragon_g_putstr_y

		; frames 81-254: delay
100		cmpa #$ff
		bne 99F
		; frame 255: done!
		jmp ttl_game_over
99		rts

		DATA

t_gameover	fci "GAME",0,"OVER",0

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		DATA

font_score	include "font-score.s"

; Number of 50s to add will be encoded in plr_score50 and processed a bit
; at a time, lowest bit first (plr_score50_idx initialised to 0).  For
; each bit, a value from this table is added if it's set, than the index
; is incremented by 3 to the next entry.  No score addition greater than
; 51150 is permitted or this will fail, but that should be fine - the
; largest single increment I expect is way less than that.

bin50s_to_bcd	fcb $00,$00,$50
		fcb $00,$01,$00
		fcb $00,$02,$00
		fcb $00,$04,$00
		fcb $00,$08,$00
		fcb $00,$16,$00
		fcb $00,$32,$00
		fcb $00,$64,$00
		fcb $01,$28,$00
		fcb $02,$56,$00
		fcb $05,$12,$00

		CODE

		; push stack up one line
raise_stack	ldx plr_stck,u
		leay 3,x
		ldb #stack_nlines-1
		stb tmp0
10		ldd stack_width,y
		std ,y++
		ldd stack_width,y
		std ,y++
		ldd stack_width,y
		std ,y++
		ldd stack_width,y
		std ,y++
		ldd stack_width,y
		std ,y
		leay 5,y
		dec tmp0
		bne 10B
		; generate purely random garbage for now
		ldd #$cf0a
!		sta ,y+
		decb
		bne <
		lda plr_grb_offset,u
		; value $80 is important - it's both on-screen black, and
		; -128, which is stored in the playfield redraw flag and
		; used as a screen offset.
		ldb #$80
		stb a,y
		stb plr_f_scrn,u	; flag playfield redraw
		lda garbage_style	; easy garbage?
		beq >			; yes? don't generate new offset
		ldb #10
		jsr rnd_num
		inca
		nega
		sta plr_grb_offset,u
!
		rts

; Test anything that needs updated outside the critical section.

; Entry:
; 	U -> player

update_player

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		lda plr_f_scrn,u	; playfield redraw requested?
		beq 90F			; no? skip it then
		;clr plr_f_scrn,u

		; this redraws the entire visible playfield from stack
		; data.  it's only called when the stack's been raised by
		; receiving garbage.
		ldx plr_stck,u
		leay stack_nlines*stack_width,x
		ldx plr_scrn,u
		leax 17*32+19*128+12,x
		adda #64
		sta plr_f_scrn,u
		leax a,x
		ldb #20
		stb tmp0
20		ldd ,--y
		std ,--x
		std -32,x
		ldd ,--y
		std ,--x
		std -32,x
		ldd ,--y
		std ,--x
		std -32,x
		ldd ,--y
		std ,--x
		std -32,x
		ldd ,--y
		std ,--x
		std -32,x
		leax -118,x
		leay -3,y
		dec tmp0
		bne 20B

		jsr draw_piece
		jsr draw_ghost

90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		lda plr_winner,u
		beq 90F
		ldx plr_scrn,u
		leay 8*192+5,x
		ldx #t_winner
		jsr dragon_g_putstr_y
		leay 188,y
		jmp dragon_g_putstr_y
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; WIP: add 50s, a bit at a time
		ldb plr_score50_idx,u
		lsr plr_score50,u
		ror plr_score50+1,u
		bcc 90F
		ldx #bin50s_to_bcd
		abx
		lda plr_score+3,u
		adda 2,x
		daa
		sta plr_score+3,u
		lda plr_score+2,u
		adca 1,x
		daa
		sta plr_score+2,u
		lda plr_score+1,u
		adca ,x
		daa
		sta plr_score+1,u
		lda plr_score,u
		adca #$00
		daa
		sta plr_score,u
90		addb #3
		stb plr_score50_idx,u

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; If remaining lines count is <= 0, bump the level.
		lda plr_lvl_lines,u
		deca
		bpl 90F
		inc plr_level,u
		jsr update_level
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		lda plr_f_drawheld,u
		beq 90F
		clr plr_f_drawheld,u
		ldx plr_scrn,u
		leay 8*32,x
		ldx plr_pc_held,u
		ldd pc_preview,x
		std ,y
		std 32,y
		ldd pc_preview+2,x
		std 64,y
		std 96,y
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; Scroll next piece previews if necessary

		lda plr_f_drawnext,u
		beq 90F

		clr plr_f_drawnext,u
		ldx plr_scrn,u
		leay 16*32+13,x
		lda #5
		sta tmp0
20		lda #4
		sta tmp0+1
10		ldd 6*32,y
		std ,y
		leay 32,y
		dec tmp0+1
		bne 10B
		leay 2*32,y
		dec tmp0
		bne 20B

		ldx plr_pcq+6*2,u
		ldd pc_preview,x
		std ,y
		std 32,y
		ldd pc_preview+2,x
		std 64,y
		std 96,y
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; Clear down own garbage.  Note sending remaining is done
		; separately so as not to bias it by player.  This very
		; simply reduces or clears each garbage queue entry in
		; turn.  Zeroes at the beginning of the queue after this
		; are fine, as it'll flag the need to start a new timer
		; for the next entry.

		lda plr_grb_send,u	; any in the send queue?
		beq 90F
		sta plr_f_grb,u		; flag queue redraw
		sta tmp0
		suba plr_grb_total,u
		bmi 10F
		; more (or as many) sent than there were lines in the
		; queue: just reset the queue and move on
		sta plr_grb_send,u
		clr plr_grb_total,u
		clr plr_grb_head,u
		clr plr_grb_tail,u
		clr plr_grb_timer,u
		clr plr_grb_timer+1,u
		bra 90F
		; more lines in the queue than we are sending: work
		; through the queue
10		clr plr_grb_send,u
		nega
		sta plr_grb_total,u
		;
		leax plr_grb,u
		ldb plr_grb_head,u
		lda tmp0
20		suba b,x
		bmi 30F		; done send, lines left at current pos
		incb
		andb #$0f
		tsta
		beq 40F
		bra 20B
		;
30		nega
		sta b,x
		; update head and reset timer if necessary
40		cmpb plr_grb_head,u
		beq 90F
		stb plr_grb_head,u
		; 12 * entry delay (usually 6s)
		clra
		ldb plr_lvl_entr,u
		lslb
		addb plr_lvl_entr,u
		lslb
		lslb
		rola
		std plr_grb_timer,u
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; Check garbage timer, if it decrements to zero then
		; receive garbage from queue.
		ldd plr_grb_timer,u	; is there a valid timer running?
		beq 90F			; if not, the queue is empty
		subd #1
		std plr_grb_timer,u
		bne 90F
		; Timer was valid and has been decremented to zero -
		; receive garbage from head and advance head.
		ldb plr_grb_head,u
		leax plr_grb,u
		lda plr_grb_total,u
		suba b,x
		sta plr_grb_total,u
		lda b,x
		sta plr_grb_recv,u
		incb
		andb #$0f
		stb plr_grb_head,u
		sta plr_f_grb,u		; flag queue redraw, a != 0
		cmpb plr_grb_tail,u	; head == tail (no more queued)?
		beq 80F			; yes? skip timer reset
		; 12 * entry delay (usually 6s)
		clra
		ldb plr_lvl_entr,u
		lslb
		addb plr_lvl_entr,u
		lslb
		lslb
		rola
		std plr_grb_timer,u
		; generate new random cell
80		ldb #10
		jsr rnd_num
		inca
		nega
		sta plr_grb_offset,u
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; Always draw the first queue element if present, as it
		; may change colour based on timer value.  Only draw the
		; rest if flagged.

		lda #36
		sta tmp0
		ldb plr_grb_head,u
		stb tmp1
		ldx plr_scrn,u
		leay 19*128+17*32+1,x
		cmpb plr_grb_tail,u
		beq 25F
		leax plr_grb,u
		; First entry is drawn green, yellow, red according to
		; timer.
		ldb plr_grb_timer,u
		bne 5F
		lda #$bf		; red
		ldb plr_grb_timer+1,u
		subb gravity_curve
		bcs 10F
		subb gravity_curve
		bcs 10F
		lda #$9f		; yellow
		subb gravity_curve
		bcs 10F
		subb gravity_curve
		bcs 10F
5		lda #$8f		; green
10		ldb tmp1
		ldb b,x
20		sta ,y
		sta -64,y
		leay -128,y
		dec tmp0
		dec tmp0
		decb
		bne 20B
		ldb tmp1
		incb
		andb #$0f
		stb tmp1

25		lda plr_f_grb,u
		beq 90F
		clr plr_f_grb,u

		; Remaining entries drawn in white.
30		cmpb plr_grb_tail,u
		beq 50F
		ldb b,x
		lda #$80
		sta ,y
		leay -64,y
		dec tmp0
		lda #$cf		; white
40		sta ,y
		sta -64,y
		leay -128,y
		dec tmp0
		dec tmp0
		decb
		bne 40B
		ldb tmp1
		incb
		andb #$0f
		stb tmp1
		bra 30B

		; Now fill the rest with black.
50		lda #$80
60		sta ,y
		leay -64,y
		dec tmp0
		bne 60B
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; update nlines
		ldx plr_nlines,u
		cmpx plr_nlines_old,u
		beq 90F
		stx plr_nlines_old,u
		ldx plr_scrn,u
		leay 14*192+13,x
		leax plr_nlines,u
		ldb #3
		jsr [vdg_g_putbcd]
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; Draw score.  We only draw 7 of the available 8 digits,
		; but I think that's ok given the kills screens.  For each
		; byte of BCD, if either nibble has changed, we draw both
		; (and then quit - other byte changes can be drawn in
		; subsequent frames).  There's just no point in tracking
		; individual nibbles.

		lda plr_score_colr,u
		sta tmp1

		stu tmp2		; preserve U
		ldd #$0004		; nolz flag, byte count
		std tmp0
		ldx plr_scrn,u
		leay 1,x
		leax plr_score,u
		bra 25F			; skip initial digit

5		lda ,x
		bita #$f0		; is this zero?
		bne 10F			; no? set nolz flag & test changed
		; is zero
		ldb tmp0		; is nolz flag set?
		beq 20F			; no? don't draw
		;bra 15F			; yes? test byte anyway
		fcb $8c			; cmpx nop 2 bytes
10		inc tmp0		; not zero: set nolz flag
15		cmpa sizeof_score,x	; has this byte changed?
		beq 20F			; no? don't draw
		lsra
		lsra
		lsra
		lsra
		bsr drawscore
20		leay 2,y

25		lda ,x
		bita #$0f		; is this zero?
		bne 30F			; no? set nolz flag & test changed
		; is zero
		ldb tmp0+1		; is this the last byte?
		decb
		beq 35F			; yes? then test nibble anyway
		ldb tmp0		; is nolz flag set?
		beq 40F			; no? don't draw
		fcb $8c			; cmpx nop 2 bytes
		;bra 35F			; yes? test byte anyway
30		inc tmp0		; not zero: set nolz flag
35		cmpa sizeof_score,x	; has this byte changed?
		beq 40F			; no? don't draw
		anda #$0f
		bsr drawscore
40		leay 2,y

		lda ,x+
		cmpa sizeof_score-1,x	; anything in this byte changed?
		bne 60F			; yes? ok, then we're done
		dec tmp0+1		; no? test another byte
		bne 5B
60		sta sizeof_score-1,x	; record the change
		ldu tmp2		; restore U
90

		; - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		; all done with update_player
		rts

drawscore	ldu #font_score
		ldb #10
		mul
		leau d,u
		ldd 8,u
		ora tmp1
		orb tmp1
		std 128,y
		ldd 6,u
		ora tmp1
		orb tmp1
		std 96,y
		ldd 4,u
		ora tmp1
		orb tmp1
		std 64,y
		ldd 2,u
		ora tmp1
		orb tmp1
		std 32,y
		ldd ,u
		ora tmp1
		orb tmp1
		std ,y
		rts

		DATA

t_winner	fci "WELL",0,"DONE",0

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

; Using sent garbage to clear down the user's own queue already happened
; during update_player.  This now sends any that remains to the other
; player.

send_garbage
		lda plr_grb_send,u	; any to send?
		beq 99F			; no? done
		ldx plr_other,u		; sending to other player!
		leay plr_grb,x
		adda plr_grb_total,x	; would their total queue
		cmpa #12		; come to more than 12?
		bls >			; no? good
		lda #12			; yes? cap it at 12-total
!		suba plr_grb_total,x	; any left to send?
		beq 88F			; no? discard and done
		sta plr_f_grb,x		; flag queue redraw
		ldb plr_grb_tail,x	; else add to tail
		sta b,y
		incb
		andb #$0f
		stb plr_grb_tail,x
		adda plr_grb_total,x
		sta plr_grb_total,x
		; 12 * entry delay (usually 6s)
		clra
		ldb plr_lvl_entr,x
		lslb
		addb plr_lvl_entr,x
		lslb
		lslb
		rola
		std plr_grb_timer,x
88		clr plr_grb_send,u
99		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

; Keyboard scanning.  This will update two state variables per player:
; plr_kbd_state will have a bit set for each key that is held down.
; plr_kbd_new will have a bit set for each key that is _newly_ pressed.
; Both sets of keys are scanned whether or not it's a 2-player game.
;
; Keys for player 1 and player 2 are scanned at as close to exactly the
; same time as possible, hopefully eliminating any possibility of an
; advantage.
;
; Any non-player key combos (eg, pause or quit game) should be checked for
; elsewhere: this code runs within the vsync period, and needs to be as
; fast as possible.

		section "DEFS"

kbit_left	equ 1<<0
kbit_right	equ 1<<1
kbit_sdrop	equ 1<<2
kbit_hdrop	equ 1<<3
kbit_racw	equ 1<<4
kbit_rcw	equ 1<<5
kbit_hold	equ 1<<6

		BSS

plr1_keys	rmb 7*2
plr2_keys	rmb 7*2

		CODE

scan_keyboard

		ldx #plr1_keys
		ldu #plr2_keys
		ldy #reg_pia0_pdra

		ldb #7
		stb tmp0

10		ldd ,x++		; player 1 keys
		sta 2,y
		orb ,y			; OR everything but test bit
		subb #$ff		; SUB sets CC.C if key down
		rol tmp1		; so SET bits = key down

		pulu d			; player 2 keys
		sta 2,y
		orb ,y			; OR everything but test bit
		subb #$ff		; SUB sets CC.C if key down
		rol tmp1+1		; so SET bits = key down

		dec tmp0
		bne 10B

		lda nplayers
		bne >
		lda tmp1
		ora tmp1+1
		sta tmp1
!

		; Anything that was 0 in the previous state and 1 in the
		; current state is newly pressed.
		;
		; new = NOT prev AND current

		lda plr1+plr_kbd_state
		coma
		sta tmp0
		lda tmp1
		sta plr1+plr_kbd_state
		anda tmp0
		sta plr1+plr_kbd_new

		bita #kbit_left
		beq 10F
		ldb #~kbit_right
		bra 20F
10		bita #kbit_right
		beq 30F
		ldb #~kbit_left
20		stb plr1+plr_kbd_mmask
30

		lda plr2+plr_kbd_state
		coma
		sta tmp0
		lda tmp1+1
		sta plr2+plr_kbd_state
		anda tmp0
		sta plr2+plr_kbd_new

		bita #kbit_left
		beq 10F
		ldb #~kbit_right
		bra 20F
10		bita #kbit_right
		beq 30F
		ldb #~kbit_left
20		stb plr2+plr_kbd_mmask
30

		rts

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

blockdown_start	equ INIT_start
blockdown_exec	equ start
		export "blockdown_start"
		export "blockdown_exec"

DP_start_	equ DP_start
DP_end		section "DP"
CODE_start_	equ CODE_start
CODE_end	CODE
DATA_start_	equ DATA_start
DATA_end	DATA
BSS_start_	equ BSS_start
BSS_end		BSS

		end start
