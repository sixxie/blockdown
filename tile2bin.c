/*

tile2bin - convert graphics to binary data suitable for Dragon/Tandy CoCo
Copyright 2014-2020 Ciaran Anscomb

Licence: GNU GPL version 3 or later <http://www.gnu.org/licenses/gpl-3.0.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

See the output of "tile2bin --help" for options.

*/

#define _POSIX_C_SOURCE 200809L

#include <ctype.h>
#include <fcntl.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef _WIN32
#include <io.h>
#endif

#include <SDL.h>
#include <SDL_image.h>

#ifdef main
# undef main
#endif

static int getpixel(SDL_Surface *surface, int x, int y);
static int getpixel_1bit(SDL_Surface *surface, int x, int y);
static int getpixel_2bit(SDL_Surface *surface, int x, int y);
static int getpixel_4bit(SDL_Surface *surface, int x, int y);

static void helptext(void) {
	puts(
"Usage: tile2bin [OPTION]... IMAGE-FILE\n"
"Convert graphics to binary data suitable for Dragon/Tandy CoCo.\n"
"\n"
"  -x PIXELS     offset to left edge [0]\n"
"  -y PIXELS     offset to top edge [0]\n"
"  -w PIXELS     width [from image]\n"
"  -h PIXELS     height [from image]\n"
"  -X            flip horizontally\n"
"  -Y            flip vertically\n"
"  -s SHIFT      initial right shift in pixels (0-7) [0]\n"
"  -W WIDTH      pixel width in source image [2]\n"
"  -H HEIGHT     pixel height in source image [1]\n"
"  -b BITS       bits per pixel (1, 2, 4) [2]\n"
"  -r            resolution graphics (same as -W 1 -b 1)\n"
"  -S            semigraphics\n"
//"  -A            semigraphics 4 (2 lines per byte)\n"
"  -m V[,V]...   map output to different values\n"
"  -M STRIDE     mask stride (-2=disable, -1=whole sprite, 0=whole line) [-2]\n"
"  -o FILE       output filename [standard out]\n"
"\n"
"  -?, --help    show this help\n"
"\n"
"Mask STRIDE > 0 is in bytes.  Mask data always precedes image data.\n"
	);
}

static struct option long_options[] = {
	{ "help", no_argument, NULL, '?' },
	{ NULL, 0, NULL, 0 }
};

int main(int argc, char **argv) {
	int xoff = 0, yoff = 0;
	int w = 0, h = 0;
	_Bool flip_horizontal = 0;
	_Bool flip_vertical = 0;
	int shift = 0;
	char *mapstring = NULL;
	int pwidth = 2;
	int pheight = 1;
	int bits = 2;
	_Bool semigraphics = 0;
	int map[16] = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15 };
	int maskstride = -2;

#ifdef _WIN32
	setmode(fileno(stdout), O_BINARY);
#endif

	int c;
	while ((c = getopt_long(argc, argv, "x:y:w:h:XYs:W:H:b:rSm:M:o:?",
				long_options, NULL)) != -1) {
		switch (c) {
		case 0:
			break;
		case 'x':
			xoff = strtol(optarg, NULL, 0);
			break;
		case 'y':
			yoff = strtol(optarg, NULL, 0);
			break;
		case 'w':
			w = strtol(optarg, NULL, 0);
			break;
		case 'h':
			h = strtol(optarg, NULL, 0);
			break;
		case 'X':
			flip_horizontal = 1;
			break;
		case 'Y':
			flip_vertical = 1;
			break;
		case 's':
			shift = strtol(optarg, NULL, 0);
			if (shift < 0 || shift > 7) {
				fprintf(stderr, "shift out of range\n");
				exit(EXIT_FAILURE);
			}
			break;
		case 'W':
			pwidth = strtol(optarg, NULL, 0);
			break;
		case 'H':
			pheight = strtol(optarg, NULL, 0);
			break;
		case 'b':
			bits = strtol(optarg, NULL, 0);
			break;
		case 'r':
			pwidth = bits = 1;
			break;
		case 'S':
			semigraphics = 1;
			bits = 4;
			break;
		case 'm':
			mapstring = strdup(optarg);
			break;
		case 'M':
			maskstride = strtol(optarg, NULL, 0);
			break;
		case 'o':
			if (freopen(optarg, "wb", stdout) == NULL) {
				fprintf(stderr, "Couldn't open '%s' for writing: ", optarg);
				perror(NULL);
				exit(EXIT_FAILURE);
			}
			break;
		case '?':
			helptext();
			exit(EXIT_SUCCESS);
		default:
			exit(EXIT_FAILURE);
		}
	}

	if (optind >= argc) {
		fputs("no input files\n", stderr);
		exit(EXIT_FAILURE);
	}

	if (bits != 1 && bits != 2 && bits != 4) {
		fprintf(stderr, "Invalid number of bits per pixel: use 1, 2 or 4\n");
		exit(EXIT_FAILURE);
	}

	SDL_Surface *in = IMG_Load(argv[optind]);
	if (!in) {
		fprintf(stderr, "Couldn't read image %s: %s\n", argv[optind], IMG_GetError());
		return 1;
	}

	if (mapstring) {
		char *ent = strtok(mapstring, ",");
		for (int i = 0; i < 16; i++) {
			if (ent) {
				map[i] = strtol(ent, NULL, 0);
				ent = strtok(NULL, ",");
			}
		}
	}

	if (!w)
		w = in->w;
	if (!h)
		h = in->h;
	int nlines = h / pheight;
	int wbytes = (((w*bits)/pwidth) + shift + 7) >> 3;  // bytes per line
	int nbytes = wbytes * nlines;  // total bytes
	if (maskstride == 0) {
		maskstride = wbytes;
	}

	uint8_t *data = malloc(nbytes);
	uint8_t *datap = data;
	uint8_t *mask = malloc(nbytes);
	uint8_t *maskp = mask;

	for (int y = 0; (y+pheight) <= h; y += pheight) {
		uint8_t d = 0;
		uint8_t m = 0xff;
		int n = shift;
		int c = 0;
		for (int x = 0; x < w; x += pwidth) {
			int getx = flip_horizontal ? (xoff + w - x - 1) : (xoff + x);
			int gety = flip_vertical ? (yoff + h - y - 1) : (yoff + y);
			int p = getpixel(in, getx, gety);
			if (semigraphics) {
				int t = (p & 32) ? 0x05 : 0x00;
				int black = p & 16;
				int s = t ? 0 : (black ? 0x00 : 0x05);
				if (!c && !t && !black) {
					c = 0x80 | ((p & 7) << 4);
				}
				d = ((d << 1) & 0x0a) | s;
				m = 0xf0 | ((m << 1) & 0x0a) | t;
				n++;
				if (n == 2) {
					if (!c)
						c = 0x80;
					n = 0;
					*(datap++) = c | d;
					*(maskp++) = m;
					c = 0;
				}
			} else {
				int t = (p & 32) ? 1 : 0;
				switch (bits) {
				case 1: c = getpixel_1bit(in, getx, gety); break;
				default:
				case 2: c = getpixel_2bit(in, getx, gety); t = t ? 3 : 0; break;
				case 4: c = getpixel_4bit(in, getx, gety); t = t ? 15 : 0; break;
				}
				c = map[c&15];
				d = (d << bits) | c;
				m = (m << bits) | t;
				n += bits;
				if (n == 8) {
					n = 0;
					*(datap++) = d;
					*(maskp++) = m;
				}
			}
		}
		if (n) {
			if (semigraphics) {
				d = (d << 1) & 0x0a;
				if (!c)
					c = 0x80;
				*(datap++) = c | d;
				m = (m & 0xf0) | ((m << 1) & 0x0a) | 0x05;
				*(maskp++) = m;
			} else {
				d <<= (8-n);
				*(datap++) = d;
				m <<= (8-n);
				m |= ((1<<(8-n))-1);
				*(maskp++) = m;
			}
		}
	}

	SDL_FreeSurface(in);
	datap = data;
	maskp = mask;

	if (maskstride < 0) {
		if (maskstride == -1)
			fwrite(maskp, wbytes, h, stdout);
		fwrite(datap, wbytes, h, stdout);
		return EXIT_SUCCESS;
	}
	for (int j = 0; j < h; j++) {
		for (int i = wbytes; i > 0;) {
			int n = (maskstride <= i) ? maskstride : i;
			fwrite(maskp, n, 1, stdout);
			fwrite(datap, n, 1, stdout);
			maskp += n;
			datap += n;
			i -= n;
		}
	}
	return EXIT_SUCCESS;
}

// Orange and black are special-cased, otherwise this maps RGB values to
// encoded VDG colours.  First element, being black, never used.

static int encode_rgb[8] = {
	0, 2, 0, 5, 3, 6, 1, 4
};

// Returns an encoded colour value from a palette of 18:
// 0-7: green, yellow, blue, red, white, cyan, magenta, orange
// 8-15: dark versions of above
// 16: black, 32: alpha

static int getpixel(SDL_Surface *surface, int x, int y) {
	if (x < 0 || y < 0 || x >= surface->w || y >= surface->h)
		return -1;

	int bpp = surface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

	Uint32 dp;
	switch(bpp) {
	case 1: dp = *p; break;
	case 2: dp = *(Uint16 *)p; break;
	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			dp = p[0] << 16 | p[1] << 8 | p[2];
		else
			dp = p[0] | p[1] << 8 | p[2] << 16;
		break;
	case 4: dp = *(Uint32 *)p; break;
	default: dp = 0; break;
	}

	Uint8 dr, dg, db, da;
	SDL_GetRGBA(dp, surface->format, &dr, &dg, &db, &da);
	// any alpha value < 128 is considered transparent
	int trans = (da < 128) ? 32 : 0;

	// special-case bright orange
	if (dr >= 0xaa && dg >= 0x55 && dg < 0xaa && db < 0x55)
		return 7 | trans;

	// special-case dark orange
	if (dr >= 0x55 && dr < 0xaa && dg >= 0x1b && dg < 0x55 && db < 0x55)
		return 15 | trans;

	int colour = 0;
	colour |= (dr >= 0x55) ? 0x04 : 0;
	colour |= (dg >= 0x55) ? 0x02 : 0;
	colour |= (db >= 0x55) ? 0x01 : 0;

	// special-case black
	if (colour == 0)
		return 16 | trans;

	int encoded = encode_rgb[colour];
	if (dr < 0xaa && dg < 0xaa && db < 0xaa)
		encoded |= 0x08;
	return encoded | trans;
}

static int getpixel_1bit(SDL_Surface *surface, int x, int y) {
	int colour = getpixel(surface, x, y);
	if (colour < 0)
		return -1;
	// return 1 for any "bright" pixel, else 0
	return (colour < 8);
}

static int getpixel_2bit(SDL_Surface *surface, int x, int y) {
	int colour = getpixel(surface, x, y);
	if (colour < 0)
		return -1;
	// works for either PMODE 3 colour set
	return colour & 3;
}

static int getpixel_4bit(SDL_Surface *surface, int x, int y) {
	int colour = getpixel(surface, x, y);
	if (colour < 0)
		return -1;
	// just strips black
	return colour & 15;
}
