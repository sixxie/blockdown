; Blockdown
; Copyright (c) 2020-2021 Ciaran Anscomb

; ROM cartridge wrapper.  Unzips the game into memory, shows the tape
; loading screen for a few seconds (skippable by keypress), then jumps to
; the game's usual start address.

		include "dragonhw.s"
		include "blockdown-rom.exp"

		; order sections
		section "DP"
DP_start	org $0074
		CODE
		setdp $00
CODE_start	org $c000

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		CODE

start
		orcc #$50

		lds #$0400

		ldd #$34ff
		tfr b,dp
		setdp $ff

		sta reg_pia0_cra	; HS disabled
		sta reg_pia1_cra	; printer FIRQ disabled
		sta reg_pia1_crb	; CART FIRQ disabled
		inca
		sta reg_pia0_crb	; FS enabled hi->lo

		ldx #$1400
		ldd #$8f8f
!		std ,x++
		cmpx #$1600
		blo <

		lda reg_pia0_pdrb	; clear FS IRQ
		sync

                lda #$08
                sta reg_pia1_pdrb
		sta reg_sam_f3s

		ldx #loadscreen_dz
		ldd #loadscreen_dz_end
		ldu #$0400
		bsr dunzip

		lda reg_pia0_pdrb	; clear FS IRQ
		sync

		lda #$09
		sta $ff9c
		sta reg_sam_v2s
		sta reg_sam_f3c

		;ldx #blockdown_dz
		ldd #blockdown_dz_end
		ldu #blockdown_start
		bsr dunzip

		clr reg_pia0_pdrb
		ldx #$0000
!		lda reg_pia0_pdra
		ora #$80
		coma
		bne >
		exg a,a
		exg a,a
		exg a,a
		leax -1,x
		bne <
!

		ldd #$ff00
		sta reg_pia0_pdrb
		tfr b,dp
		setdp $00

		jmp blockdown_exec

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		section "DP"

duz_end		rmb 2

; dunzip - unzip simply coded zip data

; 1iiiiiii 0nnnnnnn - repeat 128-n (1-128) bytes from current + 1i (-ve 2s cmpl)
; 1iiiiiii 1jjjjjjj nnnnnnnn - repeat 128-n (1-256) bytes from current + 1ij
; 0nnnnnnn - directly copy next 128-n (1-128) bytes

; entry: x = zip start, d = zip end, u = destination

		CODE

		setdp $ff

dunzip		std duz_end
duz_loop	ldd ,x++
		bpl duz_run	; run of 1-128 bytes
		tstb
		bpl duz_7_7
duz_14_8	lslb		; drop top bit of byte 2
		asra
		rorb		; asrd
		leay d,u
		ldb ,x+
		bra 10F		; copy 1-256 bytes (0 == 256)
duz_7_7		leay a,u	; copy 1-128 bytes
10		lda ,y+
		sta ,u+
		incb
		bvc 10B		; count UP until B == 128
		bra 80F
1		ldb ,x+
duz_run		stb ,u+
		inca
		bvc 1B		; count UP until B == 128
80		cmpx duz_end
		blo duz_loop
		rts

		setdp $00

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

loadscreen_dz	includebin "loadscreen-rom.bin.dz"
loadscreen_dz_end
blockdown_dz	includebin "blockdown-rom.bin.dz"
blockdown_dz_end

; - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

		align 8192,$ff
		equ *
